---
title: Wildflower
aliases: 
date: 2023-08-20
edit: 2024-06-22
notetype: feed
lang: en
tags:
  - Livres
  - Critique
  - Queer
description: Review of Wildflower
---

**Wildflower**, by Cathleen Collins  
**CW / themes :** Survival, Lesbian romance, Mental health

![It’s the cover of Wildflower, with a forest if the background and a girl hugging a dog at the front.It’s a bit of a cheap cover for a book](/assets/attachments/reviews/wildflower_cover.jpg =350x)

So it’s a preview of a title given to me when asking for it at Netgalley, to be transparent. It will not change what I’ll say about it :)

As I see it, it’s a Robinson Crusoe kind of book, at least in the first part : the survival of a girl (a woman after a while) almost alone in the forest after a plane crash.  
Then, the inevitable clash when comes the civilisation, and how Lily will adapt to it.

And of course, the romance, with a fated lover 💜​

I read the book almost in one go, and it was entertaining, but it was lacking in realism in several parts I can’t pin down now.  
There is also a huge shift near the end of the story who feels a bit surreal, made up, to let the story advance.

Nevertheless, the romance was good, even if it’s a *very* slow burn, and even if I wanted them to kiss 100 pages before, the ending is satisfying.

A lesbian Crusoe is a good idea, and I enjoyed it, but to be honest, I can’t really recommend it, it didn’t click with me and even if I’m always up to cheesy lesbian romance, there were too much part feeling surreal that I wasn’t really anchored in the story.

Other things who bothered me :  

- Being rich solve everything, yay  
- The fated lover trope

But I did really enjoyed it, it was a good distraction ^^