---
title: 2 novellas de SFF par Aeph
aliases: 
date: 2023-08-18
edit: 2024-06-22
notetype: feed
tags:
  - Note
  - Livres
  - Critique
  - Fantasy
  - SF
  - Queer
description: 2 novellas de SFF par Aeph
---

**La Sorcière, La Hyène et le garde manger**, ansi que **Green Lady** par Aeph.  langue : français.  
**CW / thèmes :** Fantasy, Romance lesbienne, parodie / Science-fiction, Romance Lesbienne, IA.  

![deux couvertures des livres présentés, La Hyène, la Sorcière et le Garde-manger nous présente une hyène violette en fond avec une meuf blanche lesbienne aux cheveux roses et side-cut au premier plan.Green Lady nous présente un couple de femmes qui s’enlacent dans un fond de ville futuriste. L’une des femmes est une femme noire aux longs cheveux à l’air un peu triste ou fatiguée, l’autre a des cheveux aux reflets verts, les yeux verts et lèvres vertes. On comprends qu’elle est un robot, sa main semble articulée comme une marionette.](/assets/attachments/critiques/greenLadySorciereHyene_covers.jpg =500x)

Pour ce Vendredi Lecture, je vous présente [2 novellas par Aeph](https://ybyeditions.fr/publisher/aeph/), assez classiques dans leur genre mais très cool à lire.

Les objets en soit sont très bien faits, Yby éditions connais bien son taff, et les couv’ en jettent.

La Hyène, la Sorcière et le Garde-manger est un conte parodique reprenant le mythe de la méchante sorcière, avec pour guise de blanche-neige une princesse lesbienne punk.

Green Lady, de son côté, reprends le mythe de l’amour avec une IA, (ça fait très penser au film Her), mais avec un twist et le fait qu’encore une fois, c’est une histoire d’amour lesbienne.

Pour être tout à fait honnête, j’ai beaucoup aimé Green Lady alors que La Hyène m’a moins parlé, malgré un essai de critique de classe (entre la princesse et son peuple vassal), ça restait une histoire très loufoque et "enfantine".  
J’ai peut-être été moins sensible au ton parodique d’une histoire qui restait adolescente dans ses thèmes, je ne saurais pas trop à quel public le conseiller.

Green Lady m’a frappée en plein cœur parce que j’ai un cœur d’artichaut bien entendu, mais pas que, en moins d’une centaine de pages Aeph nous dessine un monde certes classique mais tangible, et une histoire d’amour qui l’est tout autant. Une belle découverte qui donne envie d’en lire plusse 💜

Si vous cherchez des petits cadeaux à faire ce genre de format est parfait pour ça, franchement je conseille à tout le monde de parcourir le site de [Yby](https://ybyeditions.fr/catalogue/), beaucoup de choses donnent envie dans leur catalogue ✨  
(faut encore que je lise le recueil Histoires de Fleurs :3)

(non, ceci n’est pas un blog sponsorisé du tout, mais c’est difficile de résister à faire de la pub quand on tombe sur un boulot aussi cool !)