---
title: Critique de Kushiel
aliases: 
date: 2022-08-13
edit: 2024-06-22
notetype: feed
tags:
  - Livres
  - Critique
  - Fantasy
  - BDSM
  - TDS
  - Queer
description: Ma critique de la série Kushiel
---

**Kushiel**, de Jacqueline Carey. Langue : anglais mais une très bonne traduction française.  
**Thèmes :** Fantasy, BDSM, TDS, Bisexusalité, Polyamour.  
**CW :** Viol, torture, traite d'être humains, même si rien n'est gratuit.

![Couverture de Kushiel tome 1 - La Marque. On y vois un symbole de rose stylisé, qui est utilisé en tatouage dans la série.](/assets/attachments/critiques/kushiel_cover.jpg =350x)

Vu qu'on parle de Kushiel, c'est une série très complexe à pitcher, mais bon, on va essayer quand même o/

C'est une série d'heroïc fantasy, en trois (gros) tomes, qui prends place dans une sorte de France fantasmée, dans une époque proche de la renaissance. Les habitants de ce pays ont le sang des déesses / dieux de l'amour dans les veines, et sont donc réputés pour leur beauté et leur sensualité ^^

La mythologie du pays où ça se passe est basée sur les différentes formes d'amour / de sexe, une sorte de jésus (Élua, ici) et ses apôtres qui prôneraient chacun·e leur forme d'amour comme dogme principal. Élua lui-même n'ayant qu'un principe : "Aime comme tu l'entends".

Par exemple, tu as une des déesse (Naamah) qui a choisis d'offrir son corps à des gens lors de leur périple, pour résoudre des situation - allégorie explicite de la prostitution - un des apôtres qui était un ange punisseur qui aimait les victimes de ses punissions (Kushiel) - allégorie explicite du SM - Cassiel, qui est une sorte d'allégorie de l'amour platonique etc…

Et donc, l'héroïne, Phèdre, est une élue de Kushiel, ce qui signifie qu'elle trouve la jouissance dans la douleur. Elle est élevée par des prêtresses de Naamah - donc, de la prostitution institutionnalisée, glorifiée par un culte - , va être repérée par un noble, et sera formée à l'espionnage et à la politique. Au cours de ses missions d'espionnage, elle va finir par tomber sur un secret pouvant changer le futur du royaume, et c'est ainsi qu'une grande épopée commence…

----

Phèdre est donc une héroïne TDS, soumise et masochiste, bi et poly accessoirement, qui est très intelligente et incroyablement badass, et qui va résoudre les situations par sa ruse, ses connaissances et son courage, principalement.

L'intrigue est en outre marquée par sa relation complexe avec Mélisande, une femme très dangereuse qui, je dois le dire, a peuplé mon imaginaire érotique pendant des années x)  
L'histoire est sous-tendue par cette phrase : «Qui se soumet n'est pas toujours faible».

Franchement, que dire de plus ? C'est une œuvre incroyablement queer, dans beaucoup de sens du terme.

Alors, ça repose aussi sur beaucoup de clichés/stéréotypes, ça joue même dessus explicitement, en allant jusqu'à nommer l'héroïne comme la figure tragique par excellence, mais honnêtement, c'est très bien utilisé, et à la lecture de l'ensemble, le message qui est transmis est positif et libérateur.

Et enfin, le style est très loin d'être en reste, c'est vraiment super travaillé, la traduction est très réussie… C'est un livre qui fait partie de mes coups de cœur inconditionnels, donc bon, je ne peut que vous le conseiller :3

Dans les points négatifs, je regrette qu'il y ait trop peu de romance lesbienne, et je dois dire que ses ship hétéros m'intéressent peu (après, ce n'est pas le centre de l'histoire du tout).

On peu aussi regretter le fait que dans une société où la bisexualité est censée être 'normale', ce soit toujours le couple hétero qui domine.

Par ailleurs, c'est pas là que vous trouverez de bonnes représentations racisée, ou même pauvre à priori, on reste sur des intrigues de cours et c'est très blanc ^^'

Ah, et pleins de TW sur le troisième tome (viol, dont sur enfants, torture, pleins de trucs) , c'est très très sombre, les 3 tomes sont relativement indépendants donc c'est pas nécessaire si vous le sentez pas !

Voilou voilou, j'en avait pas encore parlé par ici donc ça me semblait nécessaire hihi, désolée pour la longueur 😇
