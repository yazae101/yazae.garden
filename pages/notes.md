---
title: Notes
layout: default
pagination:
  data: collections.feed
  size: 10
  alias: pageFeed
permalink: "notes{% if pagination.pageNumber > 0 %}-{{ pagination.pageNumber | plus: 1 }}/{% endif %}/index.html"
---
# {{ title }}

Vous trouverez ici toutes mes notes, c'est encore en travaux, de nombreuses autres devraient arriver sous peu 😅

{%- include 'feed.html' -%}


