---
title: En bonne santé
aliases: []
notetype: feed
date: 2022-12-06
edit: 2023-09-03
tags:
  - Journal
  - Handicap
description: En bonne santé… mais quand même handie.
---
*Originellement posté en thread sur le fédiverse.*

Je suis vraiment soulagée/chanceuse d’aller _bien_ en ce moment, comparativement à y’a 2 ans où c’était… tellement compliqué que mon cerveau a oublié à quel point et qu’essayer de se souvenir ressemble à se souvenir d’un cauchemard.

C’est comme les grandes douleurs, quand tu ne les subits plus c’est ensuite très difficile/impossible de se les rappeler vraiment, tu les diminue forcément, ce n’est plus imaginable.

En tout cas, en ce moment _ça va_, j’arrive à me faire à manger régulièrement, boire, dormir, faire les courses, le ménage et la vaisselle (avec qq ratés) et même sortir un peu en ville, 3-4 fois par mois, pour les trucs militants ou autre.  
Et, surtout, je n’ai plus de douleurs chroniques quasiment, ce qui est **extraordinaire**.

J’ai rangé mes médocs hier, et c’était incroyable de se dire que je n’ai pas mis mes vêtements compressifs depuis presque un an (!), je n’ai pas pris d’antidouleur depuis plusieurs mois, ou alors un doliprane ou deux et c’est tout.

De la même manière, ça fait 3 mois que je ne suis plus sous anti-psychotiques et anti-depresseurs du tout, et 6 mois+ que je prends plus rien pour les angoisses.

Bref, je pense pouvoir dire que je suis en "bonne santé" aujourd’hui, quoi que ça puisse vouloir dire. Mon état de santé est stable, en tout cas. Et, comme je le disais, je me sens incroyablement chanceuse de l’être, car je ne connais que très peu de personnes dans mon entourage qui sont dans ce cas.

Le principal problème, et l’une de mes plus grande inquiétude/peur/angoisse, c’est le fait que je sois au chômage depuis 1 an et demi, et que ma santé actuelle y soit intrinsèquement liée.

J’ai mis 1 an et demi à atteindre cet état de "rétablissement", en repos quasi-complet, sans aucune responsabilité même militante. J’ai utilisé ce chômage comme un arrêt maladie longue durée, et ça a fonctionné.

Rien n’est certain, mais je pense pouvoir rester dans cet état, le mieux auquel je puisse être à priori, tant que je ne travaille pas.

Mes droits au chômage, eux, expirerons dans +- 6 mois. Et, évidemment, je suis en "bonne santé", donc l’AAH n’est pas une option plausible…

Ça veut dire que dans 6 mois max, soit je dois vivre avec le RSA et donc perdre mon appart et vivre avec la moitié de mes revenus actuels, soit je dois trouver un moyen de travailler. Et c’est terrifiant.

Je ne suis plus vraiment capable de faire du code depuis mes burn-out, ouvrir un environnement de code me rempli d’angoisse, m’overwhelm instantanément, et même si je sais qu’avec l’habitude retrouvée je serais (peut-être) plus à l’aise, je pense que je ne suis plus suffisamment adaptable pour ce métier.

Pour l’édition numérique, c’est moins pire et j’ai déjà plus l’habitude d’y bosser, mais la quasi intégralité des postes d’executant·es (et pas de management) dans ce petit secteur sont soit des apprenti·es, soit de la sous-traitance dans des pays plus pauvres..

Bref, faut que j’apprenne un nouveau travail, en sachant que mes deux dernières tentatives en la matière se sont soldés par des burn-out successifs, mon corps qui me lâchait de plus en plus, et que j’ai perdu beaucoup de mes capacités d’adaptation et de concentration dans l’équation 🤡

Ah, et bien sûr, en gardant en tête que si je force trop je vais me retrouver dans un état pire qu’il y a 2 ans, et qu’il n’y aura peut-être plus de possibilités de "rétablissement" à ce stade. Oh, c’est sûr que si ça arrive j’aurais droit à l’AAH hein (même pas en vrai, c’est jamais sûr), mais ça sera trop tard.

J’ai longtemps eu du mal à me définir comme handicapée. Et de fait, je n’ai pas besoin d’aide _médicales_ pour avoir un état de santé stable en ce moment…

J’ai "juste" besoin de pouvoir payer mon loyer et ma bouffe & stuff sans travailler, de faire attention en permanence à ne pas trop forcer, d’avoir l’équipement adapté à la maison, de limiter mes sorties au maximum, d’aménager de grandes périodes de repos… tout en faisant gaffe à ma santé mentale et à ne pas trop m’isoler socialement.  
J’ai "juste" besoin d’avoir le moins de responsabilités possibles, de ne jamais m’occuper d’administratif, d’avoir des relations stables et des personnes qui me soutiennent… tout en ne pouvant pas les voir beaucoup et en ayant du mal à les soutenir en retour (vous êtes top, vous méritez mieux, merci d’être là.).  
J’ai "juste" besoin d’occulter que c’est une situation temporaire et que j’ai pas de solution de repli :’3

Bref, j’ai fais la paix avec l’idée d’être un élément plus ou moins improductif de la société, mais étrangement ça va pas suffire pour qu’on me laisse vivre tranquille, et je suis pas assez riche pour pouvoir vivre sans aide, donc… j’en suis là. 🤷‍♀️

J’ai beaucoup de chance, elle s’est appelée (entre autre) allocation chômage et APL pendant 1 an et demi, et elle s’amenuise à mesure que les jours passent 🤡


(oupsie, c’était un post neg en fait, c’était pas prévu mais je sais pas à quoi je m’attendais 🙃)
