---
title: Les micro-critiques de Yazae
aliases: 
date: 2023-05-18
edit: 2024-06-22
notetype: feed
tags:
  - Livres
  - Critique
description: Vous trouverez ici toutes mes critiques/review écrites en français sur Mastodon qui parlent de livres 📚
---

Vous trouverez ici toutes mes critiques/review écrites sur Mastodon qui parlent de livres 📚  

Les plus longues auront un article dédié, mais je préfère réunir tout dans une page plutôt que me disperser.

Vous pouvez aussi lire [[The micro-reviews of Yazae |mes micro-critiques en anglais]].

## Index

[TOC]

### Life is Strange (03/04/24)

![[Life is Strange (Comics)]]

### Non-Player Character (18/12/23)

![[Non-Player Character]]

### 2 novellas de SFF par Aeph (18/08/23)

![[2 novellas de SFF par Aeph]]

### La Sorcière captive - Les Faucons de Raverra T01 (19/08/22)

![[La Sorcière Captive - Les Faucons de Raverra T01]]

### Mooncakes (24/07/22)

![[Mooncakes]]

### <span lang="en">The Coldest Touch</span> (09/04/22)

![[The Coldest Touch]]

### <span lang="en">The Wandering Inn</span> (31/03/22)

![[Critique de The Wandering Inn]]

### <span lang="en">The Queen and the Woodborn</span> (25/06/21)

![[The Queen and the Woodborn]]

### Kushiel (06/06/21)

![[Critique de Kushiel]]

### Mes ruptures avec Laura Dean (24/08/20)

![[Mes Ruptures avec Laura Dean]]
