---
title: Mes Ruptures avec Laura Dean
aliases: 
date: 2020-08-24
edit: 2024-06-22
notetype: feed
tags:
  - Livres
  - Critique
  - Queer
description: Critique de Mes Ruptures avec Laura Dean
---

**Mes ruptures avec Laura Dean**, de Mariko Tamaki et Rosemary Valero-O'Connell  
**CW / thèmes :** Comics, Relation toxique, Relation lesbienne, Personnages queers  

![Couverture de Mes ruptures avec Laura Dean](/assets/attachments/critiques/lauraDean_cover.jpg =350x)

L'héroïne est sous l'emprise de la belle et populaire Laura Dean, qui pourtant la traite comme un mouchoir usagé et la jette à la moindre occasion.

On la suis donc dans son cheminement et ses difficultés à sortir de cette relation qui l'étouffe et dégrade ses rapports avec ses proches.

La bichromie utilisée donne un effet presque éthéré, réflexif au dessin de Valero-O'Connell, qui sers bien le récit.

Outre le sujet qui me semble intéressant à traiter, la force du titre repose aussi sur la belle galerie de personnages qui entourent l'héroïne et la soutiennent malgré ses difficultés à sortir de cette relation :)

Intéressant et actuel !