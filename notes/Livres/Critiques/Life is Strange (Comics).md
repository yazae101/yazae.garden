---
title: Life is Strange (Comics)
aliases: 
date: 2024-04-03
edit: 2024-06-22
notetype: feed
tags:
  - Livres
  - Critique
  - Fantasy
  - Queer
description: Critique du comics Life is Strange
---

**Life is Strange**, d’Emma Vieceli et Claudia Leonardi.  langue : anglais.  
**CW / thèmes :** Comics, Urban Fantasy, Romance lesbienne, Aventure.  

![La couverture de Life is Strange 001, où l'on vois Max et Chloé de dos, se tenant la main face à un couché de soleil, dans une Arcadia Bay dévastée par la tempête. Dans le reflet de l'eau par terre, on vois Max, seule, face à une tornade.](/assets/attachments/critiques/lifeIsStrange_cover.jpeg =350x)

J'ai enfin fini ma lecture du comics Life is Strange, par Emma Vieceli et Claudia Leonardi.

L'histoire se passe directement après le jeu, dans une timeline où Max a choisi Chloé à la fin, et où elles sont en couple (et oui, c'était la seule timeline correcte, je le savais ! 😜 )

Sauf qu'évidemment ça ne pourrait pas être simple, il y a un petit problème d'espace-temps chamboulé qui va les séparer, Max se retrouve dans une réalité où Rachel et Chloé sont ensembles et n'ont pas eu de gros drames dans leur vie, et elle va devoir team-up avec d'autres personnes à pouvoir pour avoir une chance de retrouver sa Chloé.

Franchement c'était vraiment super sympas comme lecture, on se concentre pas mal sur le développement des personnages, le comics prends le temps de traiter les traumas des persos, y'a 3 personnes queer au mètre carré et pas mal d'autres représentations cool, les nouveaux pouvoirs sont subtils et l'histoire n'est pas a propos de ça vraiment… j'ai lu beaucoup de fanfics pour essayer d'avoir une "closure" sur l'histoire originale qui était quand même pas mal frustrante, et le comics est une belle façon de mettre un point final au récit des aventures de Max & Chloé.

Bien sûr, ça ne parlera qu'aux fans, si vous n'avez pas aimé ou joué au jeu honnêtement c'est probablement pas pour vous, mais dans le cas contraire je vous assures que ça vaut le coût :3

(P.S : les trois premiers tomes sont traduits en français, si jamais)