---
title: Dynamiques antiféministe et anti-TDS au CNT
aliases: []
notetype: feed
date: 2018-12-01
edit: 2023-09-03
tags:
  - TDS
  - Féminisme
  - Militantisme
description: Résumé d’un article et partage de liens
---

Cet article est long à lire, mais il donne un aperçu assez complet des dynamiques antiféministe et anti-TDS au sein du militantisme de gauche, notamment ici au sein de la plus grande confédération syndicale anarchiste de France (le CNT).

On y parle de la dénonciation du viol d'une militante par un autre militant, qui a provoqué l'exclusion de 8 militant·e·s féministes dénonçant ce viol et le gel de leur commission antisexiste (!), sans jamais punir l'agresseur. [La transcription du communiqué est sur leur site (archive.org)](https://web.archive.org/web/20220521063314/https://cnttds31.noblogs.org/) mais pouvez le [retrouver en PDF ici](/assets/docs/notes/CNT/BalanceTonOrga-12juin2018.pdf).

Ça parle en filigrane de l'histoire de la création du premier syndicat de TDS français (le STRASS est une association), qui n'a malheureusement pas pu exister plus d'un an, notamment à cause de la putophobie et de l'antiféminisme d'acteurs militants, minoritaires, mais influents.

(conclusion de l'article à partir de : "Le 21 octobre 2017")

Sinon, il y aussi le manifeste de ce syndicat qui me semble être un support pertinent sur les sujets mêlés de l'antipatriarcat et de l'anticapitalisme en rapport aux luttes TDS :  
[CNT-TDS-Manifesto-6mars2017.pdf](/assets/docs/notes/CNT/CNT-TDS-Manifesto-6mars2017.pdf)
