# Dev notes
Quick notes when building the theme

## General links
https://kittygiraudel.com/2020/11/30/from-jekyll-to-11ty/
https://24ways.org/2018/turn-jekyll-up-to-eleventy/
https://stedman.dev/2020/04/29/make-the-jump-from-jekyll-to-javascript/
https://alexpearce.me/2020/06/jekyll-to-eleventy/

## Anchor links
[Accessible anchor links with Markdown-it and Eleventy](https://nicolas-hoizey.com/articles/2021/02/25/accessible-anchor-links-with-markdown-it-and-eleventy/)

[Code](https://github.com/nhoizey/nicolas-hoizey.com/blob/4c9e42b306a387e9533a1036a6286b7f24091ed4/.eleventy.js#L111-L176) 
```js
const slugify = require('@sindresorhus/slugify');
const markdownItAnchor = require('markdown-it-anchor');
const markdownItAnchorOptions = {
  permalink: true,
  permalinkClass: 'deeplink',
  // permalinkSymbol:
    // '<svg fill="#000000" height="800px" width="800px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 490 490" xml:space="preserve"><path d="M64.333,490h58.401l33.878-137.69h122.259L245.39,490h58.401l33.878-137.69h119.92v-48.162h-108.24l29.2-117.324h79.04v-48.162H390.23L424.108,0H365.31l-33.878,138.661H208.79L242.668,0h-58.415l-33.864,138.661H32.411v48.162h106.298l-28.818,117.324h-77.48v48.162h65.8L64.333,490z M197.11,186.824h122.642l-29.2,117.324H168.292L197.11,186.824z"/></svg>',
  permalinkSymbol:
    '<svg class="icon" role="img" focusable="false" viewBox="0 0 24 24" width="1em" height="1em"><use xlink:href="#symbol-anchor" /></svg>',
  level: [2, 3, 4],
  slugify: function (s) {
    return slugify(s);
  },
  renderPermalink: (slug, opts, state, idx) => {
    // based on fifth version in
    // https://amberwilson.co.uk/blog/are-your-anchor-links-accessible/
    const linkContent = state.tokens[idx + 1].children[0].content;

    // Create the openning <div> for the wrapper
    const headingWrapperTokenOpen = Object.assign(
      new state.Token('div_open', 'div', 1),
      {
        attrs: [['class', 'heading-wrapper']],
      }
    );
    // Create the closing </div> for the wrapper
    const headingWrapperTokenClose = Object.assign(
      new state.Token('div_close', 'div', -1),
      {
        attrs: [['class', 'heading-wrapper']],
      }
    );

    // Create the tokens for the full accessible anchor link
    // <a class="deeplink" href="#your-own-platform-is-the-nearest-you-can-get-help-to-setup">
    //   <span aria-hidden="true">
    //     ${opts.permalinkSymbol}
    //   </span>
    //   <span class="visually-hidden">
    //     Section titled Your "own" platform is the nearest you can(get help to) setup
    //   </span>
    // </a >
    const anchorTokens = [
      Object.assign(new state.Token('link_open', 'a', 1), {
        attrs: [
          ...(opts.permalinkClass ? [['class', opts.permalinkClass]] : []),
          ['href', opts.permalinkHref(slug, state)],
          ...Object.entries(opts.permalinkAttrs(slug, state)),
        ],
      }),
      Object.assign(new state.Token('span_open', 'span', 1), {
        attrs: [['aria-hidden', 'true']],
      }),
      Object.assign(new state.Token('html_block', '', 0), {
        content: opts.permalinkSymbol,
      }),
      Object.assign(new state.Token('span_close', 'span', -1), {}),
      Object.assign(new state.Token('span_open', 'span', 1), {
        attrs: [['class', 'visually-hidden']],
      }),
      Object.assign(new state.Token('html_block', '', 0), {
        content: `Section titled ${linkContent}`,
      }),
      Object.assign(new state.Token('span_close', 'span', -1), {}),
      new state.Token('link_close', 'a', -1),
    ];

    // idx is the index of the heading's first token
    // insert the wrapper opening before the heading
    state.tokens.splice(idx, 0, headingWrapperTokenOpen);
    // insert the anchor link tokens after the wrapper opening and the 3 tokens of the heading
    state.tokens.splhttps://github.com/nhoizey/nicolas-hoizey.com/blob/4c9e42b306a387e9533a1036a6286b7f24091ed4/.eleventy.js#L111-L176ice(idx + 3 + 1, 0, ...anchorTokens);
    // insert the wrapper closing after all these
    state.tokens.splice(
      idx + 3 + 1 + anchorTokens.length,
      0,
      headingWrapperTokenClose
    );
  },
}
  // Override the Markdown renderer to use link anchors
  config.setLibrary(
    'md',
    markdownIt({ html: true }).use(markdownItAnchor, { slugify: uslugify })
  ) 
```

[Are your Anchor Links Accessible?](https://amberwilson.co.uk/blog/are-your-anchor-links-accessible/#automating-accessible-anchor-links)

## excerpts

https://webbureaucrat.gitlab.io/articles/eleventy-excerpts/

https://nicolas-hoizey.com/articles/2023/03/31/how-i-built-my-own-excerpt-for-markdown-content-in-eleventy/

https://github.com/enrichdev-en/eleventy-excerpt-example/blob/main/.eleventy.js

https://keepinguptodate.com/pages/2019/06/creating-blog-with-eleventy/


Essayer de faire un truc comme ça :

Si note.data.description est nil, let preview = noteContent.slice(0, 240);


## build reset

https://github.com/11ty/eleventy/issues/2936#issuecomment-1546985032

https://github.com/kentaroi/eleventy-plugin-clean

Pour le moment, j’utilise eleventy-plugin-clean

mais il me faudrait un script bash qui fasse la même chose pour pages.

il existe aussi npm del pour clean le build, à voir.


## Search

https://github.com/dinhanhthi/eleventy-elasticlunr-js/tree/master

https://github.com/duncanmcdougall/eleventy-search-demo

https://www.belter.io/eleventy-search/