---
title: The Coldest Touch
aliases: 
date: 2022-04-09
edit: 2024-06-22
notetype: feed
tags:
  - Note
  - Livres
  - Critique
  - Queer
  - Fantasy
description: Critique de The Coldest Touch.
---

**The Coldest Touch**, de Isabel Sterling. Langue : anglais.  
**CW / thèmes :** Meurtres, Romance lesbienne, Vampires, Bit-lit, YA.

![Couverture de The Coldest Touch](/assets/attachments/critiques/theColdestTouch_cover.jpg =350x)

J'ai terminé **The Coldest Touch**, et j'avoue que je suis déçue, un manque de cohérence du début à la fin, des enjeux beaucoup trop importants pour des ados immatures as fuck (surtout Claire, insupportable), des fils d'intrigue non résolus comme si on les avait oubliés, bref c'était assez décevant, j'ai lu des fanfics/bouquins amateurs mieux écris.

On parle d'un 'twilight lesbien' sur Goodread mais la vérité c'est que Twilight est mieux écris en comparaison !🤷‍♀️

J'ai pas réussi à croire à l'univers posé, et ça m'arrive rarement dans mes lectures… Pourtant y'a un personnage trop cool qui utilise le pronom They, c'est bien une romance lesbienne avec des vampire, mais voilà, je n’arrive pas à le conseiller 😓