---
publish: 
title: Nom de domaine sur mon blog
aliases:
  - Nom de domaine sur mon blog - Gandi et Github Pages
  - Gandi & Github Pages
notetype: feed
date: 2022-07-26
edit: 2024-06-21
tags:
  - Wiki
  - Tech
  - Blog
description: La mise en place du nom de domaine sur mon blog, avec Gandi et Github Pages.
---
*Une note un peu datée puisque je n’utilise plus ni github pages ni Gandi depuis leur rachat, mais sait-on jamais, ça peut toujours servir.*

Je me suis rendu compte que c'était pas si facile de trouver les infos et savoir quoi faire, donc après pas mal d'essais et d'erreur, ça me semble intéressant de mettre la procédure ici :)

Pour ce tuto, j’utilise Jekyll via Github Pages, et Gandi comme fournisseur de nom de domaine.

## A - Enregistrement du nom de domaine sur Gandi

1. Aller sur [le shop de Gandi](https://shop.gandi.net/fr/5d1cd542-ab7f-11ec-8372-00163e816020/domain/suggest) pour les noms de domaines
2. Choisir son nom de domaine et l'acheter (pour un an minimum)

## B - Paramétrage du nom de domaine

1. Aller sur nom de domaine sur l'espace client de Gandi
3. Onglet "Enregistrement DNS"
4. On va créer une redirection Ipv4 (A) et ipv6 (AAAA) vers les adresses ip listées
    - Création de la redirection ipv4  
![Screen de l'interface Gandi pour l'ajout de la redirection DNS A](/assets/attachments/wiki/screen-DNS-A.jpg =400x)
    - Création de la redirection ipv6  
![Screen de l'interface Gandi pour l'ajout de la redirection DNS AAAA](/assets/attachments/wiki/screen-DNS-AAAA.jpg =400x)
1. Supprimer l'adresse ip déjà enregistrée  
![Screen de l'interface Gandi listant les enregistrements DNS. La première ligne AAAA est barrée.](/assets/attachments/wiki/screen-liste-DNS.jpg)
3. Créer une redirection CNAME vers `nom-de-compte.github.io.` (ne pas oublier le `.` à la fin !)  
![Screen de l'interface Gandi pour l'ajout de la redirection DNS CNAME](/assets/attachments/wiki/screen-DNS-CNAME.jpg)

## C - Paramétrage de Github Pages

1. Aller sur votre projet Github contenant votre site Jekyll
2. Onglet Settings, section Pages
3. Activer Github Pages en selectionnant la source (branche) du site.
4. Custom domain : entrer votre nom de domaine (sans le www.)
5. Attendez que Github vérifie les DNS et génère un certificat TLS  
![Screen de l'interface Github Pages pour activer](/assets/attachments/wiki/screen-github-pages.jpg)
6. Si les étapes précédentes ont été suivies correctement, on devrait pouvoir activer "Enforce HTTPS" et accéder correctement à notre site Jekyll depuis l'adresse du nom de domaine ! Bravo :)
