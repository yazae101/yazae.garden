On démarre de Eleventy-Garden pour construire notre blog perso sur Eleventy.

## Installation


`npm install`

```bash
added 328 packages, and audited 329 packages in 12s

26 packages are looking for funding
  run `npm fund` for details

8 vulnerabilities (3 moderate, 4 high, 1 critical)

To address issues that do not require attention, run:
  npm audit fix

To address all issues (including breaking changes), run:
  npm audit fix --force

Run `npm audit` for details.
```

`npm audit fix`

```bash
added 4 packages, removed 14 packages, changed 17 packages, and audited 319 packages in 6s

29 packages are looking for funding
  run `npm fund` for details

# npm audit report

liquidjs  <10.0.0
Severity: moderate
liquidjs may leak properties of a prototype - https://github.com/advisories/GHSA-45rm-2893-5f49
fix available via `npm audit fix --force`
Will install @11ty/eleventy@2.0.1, which is a breaking change
node_modules/liquidjs
  @11ty/eleventy  <=2.0.0-canary.18
  Depends on vulnerable versions of liquidjs
  node_modules/@11ty/eleventy

2 moderate severity vulnerabilities

To address all issues (including breaking changes), run:
  npm audit fix --force
```


`npm audit fix --force`

```bash
npm WARN using --force Recommended protections disabled.
npm WARN audit Updating @11ty/eleventy to 2.0.1, which is a SemVer major change.

added 32 packages, removed 135 packages, changed 21 packages, and audited 216 packages in 8s

38 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

## serve en local

`npx @11ty/eleventy`

`npx @11ty/eleventy --serve`

## utiliser les scripts npm

`npm start` -> permet de lancer sass et eleventy --serve en même temps

`npm run build` build eleventy & sass sans serve

