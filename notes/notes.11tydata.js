module.exports = {
    layout: "note.md",
    type: "note",
    permalink: "/notes/{{ title | slugify }}/index.html"
}
