---
title: Mooncakes
aliases: 
date: 2022-07-24
edit: 2024-06-08
notetype: feed
tags:
  - Note
  - Livres
  - Critique
  - Fantasy
  - Queer
description: Critique de Mooncakes.
---

**Mooncakes**, de Suzanne Walker & Wendy Xu.  Langue : anglais.  
**CW / thèmes :** Comics, Urban Fantasy, Sorcières, Loup-garous, Romance Queer, Rupture familiale.

![La couverture du comics Mooncakes, avec une jeune sorcière qui à l’air d’avoir préparé des gâteaux, et une personne louve-garou qui est en train de lécher le plat ayant servi à faire les gâteaux.](/assets/attachments/critiques/mooncakes_cover.jpg =350x)  

J’ai lu Mooncakes aujourd’hui, et je suis très contente de cette découverte !

C’est super mignon même si un peu naïf, l’histoire d’une ado sorcière qui aide ses grand-mères dans leur boutique de sorcellerie, et qui va aider Tam, un·e loup-garou ami·e d’enfance (qui utilise le pronom They) et potentiel crush à combattre un démon de la forêt.

Iels sont trop mignon·nes ensembles, Tam est super cool, et Nova aussi, elle porte des appareils auditifs et c’est bien utilisé dans l’histoire, les grand-mères sorcières lesbiennes sont trop cools, bref, y’a de la representation, c’est super mignon, y’a de la magie et des démons, que demander de plus ? 🥰

En plus le dessin est très réussis, des couleurs pastels qui marchent bien avec l’histoire et l’ambiance du récit, bref, je conseille cette lecture très fortement :3