---
aliases: []
title: Les critiques érotiques de Yazae
notetype: feed
date: 2022-12-22
edit: 2023-09-12
tags:
  - Livres
  - BDSM
  - Sexe
description: Vous trouverez ici toutes mes critiques/review écrites sur Mastodon qui concernent des lectures érotiques. 📚🥵📚
---
Vous trouverez ici toutes mes critiques/review écrites sur Mastodon qui concernent des lectures érotiques. 📚🥵📚  
Les CW seront marqués au début de chaque post.

[[Yazae's erotic microblogging|Les critiques en anglais sont trouvables ici]].

## Index

[TOC]

### It's even worse than it looks (23/05/22)

**It's even worse than it looks** par unknown_knowns. Langue : EN.  

**CW / thèmes :** F-f, BDSM, cute couple, Life is Strange, Victoria-Max, edge play, exhib.  

Une fanfic très cute qu'on m'a conseillée pour le côté BDSM, mais qui est vraiment cool niveau interactions et évolution des personnages, on crois au couple que Victoria et Max commencent à construire, puis Chloé et Rachel se pointent et mettent leur grain de sel, c'est drôle et cool, les scènes NSFW sont hot, franchement je conseille :3

[https://archiveofourown.org/works/8226214/chapters/18853291](https://archiveofourown.org/works/8226214/chapters/18853291 "https://archiveofourown.org/works/8226214/chapters/18853291")  

### Fragile! Handle With Care (14/12/20)

**Fragile! Handle With Care**, by KinkTailedKitty. Langue : EN.  

**CW / Thèmes :** Past abuse, Past rape, Past underage, BDSM, f-F, Petplay, Exhib, Space-fantasy  

Ma découverte obsédante du moment, et c'est pas du non-consent, c'est même plutôt fluff/mignon, incroyable ! :')  

C'est l'histoire de Mirri, une jeune succube qui se prostitue pour survivre et qui galère, et de Lillian, une incube plutôt riche qui va la prendre sous son aile en échange d'un peu de compagnie.  

Par un bail de réincarnation un peu cheloue, Mirri est le portrait craché de l'ancienne "pet"/esclave de Lillian, ce qui va causer un trouble entre les deux femmes.  

On a affaire ici aux 15 premiers chapitres d'une romance BDSM qui prends son temps, dans un univers de space-fantasy plutôt bien construit. Même si il s'agit d'un titre sexy, il n'y a pas du sexe à toutes les pages non plus, l'artiste n'hésite pas à développer ses personnages et son univers :)

Dans ce monde, les succubes sont réduits en esclavage, leur espèce est faite pour le sexe et Mirri est présentée comme "instinctivement" soumise, mais la relation entre les deux femmes est une relation consensuelle, contrairement à d'autres exemples présentés dans l'histoire (certains passages peuvent être trigger, cf les CW).  

Les derniers chapitres ouvrent l'univers et laissent entrevoir de nombreuses possibilités d'intrigues, j'aime beaucoup et je suis très impatiente de lire la suite :3

(la publication est toujours en cours et le dernier chapitre date de moins de 2 semaines)

[https://archiveofourown.org/works/26657947?view_full_work=true](https://archiveofourown.org/works/26657947?view_full_work=true)  

### Confisekse, e03 - Je t'embrasse (03/12/20)

Confisekse, e03 - **Je t'embrasse**  

J'ai découvert ce podcast aujourd'hui pendant que je bossais, j'étais pas prête :')  

C'est de la lecture de sextos échangés pendant le premier confinement, et c'est super hot, je savais pas que ça pouvait me faire autant d'effet l'audio

Bon le fait que ça soit une dom et une sub les "autrices" de cet épisode a bien aidé évidemment, c'est trash juste comme il faut, parfait :333

[https://soundcloud.com/confisekse/episode-3-je-tembrasse](https://soundcloud.com/confisekse/episode-3-je-tembrasse)  

### InSEXts (11/11/20)

**InSEXts**, de Marguerite Bennet et Arienna Kristantina. Langue : FR traduit de l'anglais.  
Tome 1 sortis en France chez Snorgleux Comics.

**CW / Thèmes :** Viol, Violences conjuguales, Meurtres violents, Relation lesbienne

<img src="../assets/img/lewd/insexts_cover.jpg" alt="Couverture du livre InSEXts, tome1.  
Deux femmes enlacées, en robe époque victorienne, entourées d'insectes et de mauvaises herbes.  
L'une des femmes est de face, et l'on vois ses yeux briller d'une lumière verte, inquiétante.  
Tons pastels, d'époque." width="50%"/>

Je triche un peu ce soir, il ne s'agit pas d'une histoire où le sexe est prédominant, même si ça reste un titre "adulte". M'enfin, y'a sex dans le titre, et y'a du sexe dans l'histoire, alors chut :p

InSEXts, c'est l'histoire d'un couple de sorcières lesbiennes à l'époque victorienne, qui combattent pour leur indépendance et fracassent du monstre (et des mascus) en aidant les veuves, les putes et les orphelins. Sacré pitch, non ? x)

Mais Lady et Mariah sont une espèce assez particulière de sorcière : leurs pouvoirs sont liés aux insectes, ou, plus exactement, elles se transforment petit à petit en insectes à mesure qu'elles gagnent en pouvoir.

Les transformations de Lady, notamment, sont de plus en plus impressionnantes au fil de l'histoire, et l'aspect monstreux et incontrôlable de ce pouvoir amène une tension intéressante dans le récit : on fini par avoir plus peur d'elle / pour elle que des antagonistes, pourtant monstrueux, que les deux femmes sont amenées à combattre.

Honnêtement, le livre n'est pas dénué de défauts : les actions sont souvent brouillonnes et l'enchaînement entre les planches pas toujours clair. L'histoire semble précipitée, au minimum. Le discours féministe assumé pourrait gagner en subtilité, aussi. De la même manière, et même si ce sont des femmes qui sont à la création, on a quand même l'impression d'un male gaze persistant tout au long du réçit, comme si l'histoire était destinée à un public masculin.

Mais les dessin, la couleur et l'ambiance sont très réuissis, et c'est dans tous les cas une curiosité que je suis contente d'avoir découverte ^^

### Erin's Morning (07/11/20)

**Erin's Morning** par VirginiaMarieAndrews. Langue : EN.  

**CW/ Thèmes :** Non-consent, Mind control, Bod mod, Slavery, BDSM, f-F  

Un court voyage (une matinée) dans la tête d'Erin, esclave et animal de compagnie d'une reine dans un futur plus ou moins proche.  

La narration est plutôt subtile, des explications sur l'univers sont disséminées tout au long du récit et pimentent une situation déjà chaude de base.  
Erin n'a pas choisie son sort, mais elle est bien dressée et a accepté sa place : nue, agenouillé aux pieds de sa reine.  

J'apprécie particulièrement les détails sur son conditionnement et ses modifications corporelles (les implants et le goût sont particulièrement savoureux).  

Une histoire très sympa dans un univers qui pourrait accueillir une myriade d'autres histoires, mais qui se suffit très bien à elle même en l'état :)

Note : j'ai aussi parlé de l'autrice dans une autre critique [ici, en anglais](https://yazae.me/notes/Yazae's-erotic-microblogging-(EN)#penny-and-megan-021221).

[https://www.literotica.com/s/erins-morning](https://www.literotica.com/s/erins-morning)  

### Blood and Lust (07/11/20)

**Blood and Lust** de Mactosh. Langue : EN.  

**CW / Thèmes :** blood, D/s, f-F

Une petite histoire courte toute mignonne entre une vampire soumise et sa dom :3

Plutôt hot même si un peu trop court à mon goût, on a jamais assez de vampiiires !

[https://www.literotica.com/s/blood-and-lust-4](https://www.literotica.com/s/blood-and-lust-4)  

### Alfie (07/11/20)

**Alfie**, de InCase. Lang : EN.    

**CW / Thèmes :** Relations libres, BDSM (light), Polyamour, Homo / bi sexualités, Xénophobie, différence en générale, pour les sujets les plus évidents.  

<img src="../assets/img/lewd/alfie-1.jpg" alt="Planche en noir et blanc, une discussion entre mère et fille, intime" width="45%"/> <img src="../assets/img/lewd/alfie-2.jpg" alt="Planche en couleur, teintes rouges. Décor : forêt rouge, étrange, inquiétante.  
Rencontre avec Alfie, une Havlin, très petite, oreilles pointues, et Ozge, une Voch'Khari, peuple très grand et élancé, avec une longue queue et des cornes, vivant dans la forêt." width="45%"/>

Une saga de fantasy toujours en cours (950+p !), avec pas mal de smut dedans ^^  

L'auteur fait du webcomic érotique à la base, et ça se vois dans le dessin (très réussis) et les thèmes (focus évident sur les corps féminins, nu de préférence, et majoritairement plantureux), mais c'est étonnament riche en nuance et ça fait partie de ces webcomics basés sur le sexe qui développent une sacré mythologie avec le temps x)  

On suit principalement les aventures d'Alfie, mais aussi de sa mère, dans un monde de fantasy relativemment classique mais bien construit.  
Elles font partie d'un peuple qui ressemble à un croisement entre des nains et des elfes, et vivent dans un petit village assez traditionnel [puritanisme de merde], autant dans les idées que dans le mode de vie.  

Une caravane d'humains et d'elfes fait escale chez eux, et c'est l'occasion pour elles de bouleverser leurs habitudes et de découvrir de nouvelles choses… dont pas mal de sexe, oui, car c'est une composante non négligeable du récit ^^  

Mais ce qui conserve notre intérêt sur autant de pages, c'est l'écriture des personnages et leur développement. Autant la relation conflictuelle mère-fille que la découverte de nouveaux désirs ([léger spoiler : la découverte de la bisexualité de l'héroïne va être un élément important du récit]), l'envie de découvrir le monde, etc, tout est très bien amené, sans forcer les choses, le récit prends son temps. C'est sensible, souvent drôle, bref, ça se lit vraiment tout seul.  

Honnêtement, je suis fan x)  

À savoir : les ~400 premières pages sont en N&B, le reste est en couleur.

[https://buttsmithy.com/archives/comic/p1](https://buttsmithy.com/archives/comic/p1)  

### The Rock Cocks (06/11/20)

**The Rock Cocks**, par Brad & Leslie Brown. Lang : EN.  

<img src="../assets/img/lewd/the-rock-cocks.png" alt="Texte : we're the ROCK COCKS  
Planche du webcomics du même nom, qui présente le groupe et le concept de la série : un couple à moitié à poil sur scène (vêtements déchirés, volontairement sexy), une meuf badass (cheveux blancs, tatouage), et du rock (le mec est à la batterie et la meuf à la guitare et au chant)  
Autour, une foule (mixte) ambiancée et un poil excitée par le spectacle" width="50%"/>

Le démarrage d'un groupe de rock qui repose sur un couple (het) "un peu" chaud et bien exhib sur les bords :')  

Y'a du cul, de l'humour, et des personnages plutôt cool (représentation trans, bi, lesb, gay, POC…). Par contre, la subtilité repassera en de nombreuses occasions (même si ça s'arrange un peu avec le temps).  

Ça a déjà un peu de bouteille donc y'a pas mal à lire et ça peut valoir le coup d'œil :)  

Puis, faut avouer, on me vends une meuf badass et queer-codé en lead character, du sexe et des dessins qui en jettent, ça me fait presque oublier que c'est principalement hétéro :3

[http://rockcocks.slipshine.net/comic/page-1-nsfw-track-1-start](http://rockcocks.slipshine.net/comic/page-1-nsfw-track-1-start)  

### My Roomate Is a Camgirl (06/11/20)  

**My Roomate Is a Camgirl**, de InsomniacArrest. Langue : EN

Une histoire d'amour très mignone et assez chaude mine de rien, avec pas mal de power-play dans les scènes de sexe même si ça reste très soft.

On reste sur de la romance lesbienne slowburn classique, avec sa dose de drama, c'est pas un roman érotique en tant que tel (le sexe n'est pas l'élément principal de l'histoire)

C'était une découverte très agréable, je recommande🙂

[https://tapas.io/series/My-Roommate-is-a-Camgirl](https://tapas.io/series/My-Roommate-is-a-Camgirl)
