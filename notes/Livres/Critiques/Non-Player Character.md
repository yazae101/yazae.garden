---
title: Non-Player Character
aliases: 
date: 2023-12-18
edit: 2024-06-22
notetype: feed
tags:
  - Note
  - Livres
  - Critique
  - Fantasy
  - LitRPG
  - Queer
description: Critique de Non-Player Character
---

**Non-Player Character**, de [Veo Corva](https://mastodon.art/@vicorva).  langue : anglais.  
**CW / thèmes :** Fantasy, LitRPG, Asexualité, Cosy Fiction.  

![Image de couverture de Non-Player Character. Dans une forêt aux tons bleus et violets, un personnage aux cheveux blancs s'approche l'air curieux d'une créature qui flotte près d'iel](/assets/attachments/critiques/NonPlayerCharacter_cover.jpg =350x)

Aujourd'hui, après une énième relecture, je voulais vous parler de **Non-Player Character**.

C'est une œuvre de Portal fantasy / LitRPG, qui rentre dans le champ de ce qu'on pourrait appeller 'cosy fiction', des œuvres résolument optimistes, bienveillantes avec leurs personnages et leur lectorat. Comme Corva le dit iel-même :

> Je pense que si tu tombe amoureux des personnages et de l'univers, alors toute menace envers elleux, quelle que soit sa taille, va paraître monumentale 

Et c'est bien ce qui se produit à la lecture de l'ouvrage : on a beaucoup d'affection pour Tar et sa famille choisie /ses camarades de jeux de rôle, on empathise à mort avec ses crises d'angoisses et sa gêne perpétuelle vis à vis de l'espèce humaine, et les enjeux n'ont pas besoin d'être le sauvetage du monde ou la tuerie de millier d'ennemis intuables pour que leur périple dans un monde de fantasy soit merveilleux, fascinant et pleins de moments forts.

Justement, ça fait du bien d'avoir des œuvres où la résolution des conflits ne se fait pas systématiquement par la violence, avec des personnages handis et neuroatypique dont la simple survie quotidienne est un enjeux.. Ça ressemble plus à ce qu'on vivrais moi et mes potes en cas de teleportation malencontreuse dans un monde de fantasy, avec toutes les galères que ça implique, et c'est juste génial à lire 💜
