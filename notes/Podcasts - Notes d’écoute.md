---
title: Notes d’écoute
aliases:
  - Podcasts - Notes d’écoute
notetype: unfeed
date: 2023-03-04
edit: 2023-09-03
tags:
  - Podcasts
  - Queer
description: Fiches d’écoute de podcasts.
---
Fiches d’écoute de podcasts.

Retrouvez aussi la liste des [[Podcasts|podcasts que j’écoute régulièrement]].

## Index

[TOC]

## Avis de Tempête

### HS- Expérience d'autonomie collectives (22/07/23)

Vraiment un podcast à écouter, hyper intéressant sur l'organisation collective, la radicalité, les squats bien sûr mais aussi le soin qu'on apporte à nos commus, comment on fait collectif sans reproduire constamment les même oppressions, le fait que la répression se ressens autant dans nos corps que dans les violences intra-communautaires, vastes sujets et pas de conclusions fixes évidemment mais je conseille beaucoup beaucoup et j'attends la suite avec impatience ! 💜​

## Chroniques Mutantes

### \#369 (15/07/23)

**Livres**  
_Un bref instant de splendeur_ d’Océan Viong
> Un livre sous la forme d’une lettre de l’auteur à sa mère, qui parle d’une relation gay adolescente, de pauvreté, de race, et de traumatismes familiaux. Très beau et intéressant.

### \#357 (25/02/23)

**Livres :**  
*Entre les deux il n’y a rien* - Matthieu Riboulet
> Auteur gay qui parle beaucoup du rapport entre amours et politique.  
> Le livre parle du début du militantisme de l’auteur, entre fiction et biographie, juste après les révoltes étudiantes de 68, entre la France, l’Italie et l’Allemagne.

### \#355 (19/02/23)

**Livres :**  
*Notre part de nuit* - Mariana Enriques
> Un gros pavé addictif.  
> Horreur politique / polar argentin, inspirée d’Anne Rice, Stephen King et réalisme magique.

*Les Vilaines* - Camilla (Metayer ed.)  
> Autrice trans. Une communauté de trans TDS trouvent un bébé.

## Le Feu, la Rage, l’Orage

### \#004 - Validisme et anticapacitisme (22/12/18)
http://www.pikez.space/le-feu-la-rage-lorage-4-validisme-et-anticapcitisme/

**Film :**  
https://mia-more.fr/yes-we-fuck/


### \#003 - Le Travail du Sexe (01/01/19)
http://www.pikez.space/le-feu-la-rage-lorage-3-le-travail-du-sexe/  

**Musiques :**  
*Le Corp d’Ama* - Mon Dragon

