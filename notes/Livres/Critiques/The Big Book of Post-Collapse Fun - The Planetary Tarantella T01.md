---
title: The Big Book of Post-Collapse Fun - The Planetary Tarantella T01
aliases: 
date: 2022-11-28
edit: 2024-06-22
notetype: feed
tags:
  - Livres
  - Critique
  - Post-apo
description: Review of The Big Book of Post-Collapse Fun - The Planetary Tarantella T01.
lang: en
---

**The Big Book of Post-Collapse Fun**, of Rachel Sharp.  VO : english.  
**CW / themes :** Post-apocalypse, Survivalism, Humor.

![It picture a woman with red short hairs in a scooter and a dog at her feet, surrounded by a clearing with trees](/assets/attachments/reviews/thePlanetaryTarantellaT1_cover.jpg =350x)

I just finished The Big Book of Post-Collapse Fun, written by Rachel Sharp, part one of a trilogy, and it was really funny to read!

Post-apo isn't really a fun theme most of the time, but following Mag make it an almost joyful experience, even if she struggle a lot, make dubious decisions and has the survival skills of a chihuahua at the start of her journey. The humorous tone Mag use to tell her story make all the difference, and it's a pleasure to keep up on her adventure with her dog, in a geologically instable world who seems to have swallowed most of humanity.

I recommend it to all people sick of depressing takes of sci-fi where 'men are wolfs to men' is the norm. We may not be in an optimistic futur, but there is joy to find there still, and Mag and Vet (the dog) will find it along their treacherous way.

Can't wait to read the next book!