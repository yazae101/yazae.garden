---
title: The Queen and the Woodborn
aliases: 
date: 2021-06-25
edit: 2024-06-22
notetype: feed
tags:
  - Note
  - Livres
  - Critique
  - Queer
  - Fantasy
description: Critique de The Queen and the Woodborn.
---

**The Queen and the Woodborn**, de Shiniez. Langue : anglais.  
**CW / thèmes :** Webtoon, Fantasy médiévale, Romance lesbienne.

![Couverture de The Queen and the Woodborn. On y vois une femme à la peau légèrement viollette avec des cornes en os étreindre et sourire à une autre femme qui a l’air étonnée.](/assets/attachments/critiques/theQueenAndTheWoodborn_cover.jpg =350x)  

Bonjour, je ne vous ai pas encore parlé de <span lang="en">The Queen and the Woodborn</span>, une des nouvelles œuvre de Shiniez (Sunstone)?

On est dans une fantasy médiévale assez classique : c'est l'histoire d'une reine et d'une mère, qui, pour sauver son fils va s'aventurer dans une forêt mythique et interdite, quitte à mettre en danger son âme. Elle y rencontre une (très belle) sorcière, qui va lui sauver la mise, mais qui attends peut-être quelque chose d'elle en retour…

Le début d'une romance tragique ? quelque chose de plus léger ? de plus épique ? Aucune idée ! L'histoire ne fait pour le moment que 5 chapitres et l'auteur prends beaucoup de temps sur le rythme de sorties.

Mais, comme d'hab, de très beaux dessins, des personnage bien écris, une narration un poil trop verbeuse, en tout cas les ingrédient sont là et la potion semble fonctionner 😊

Tout comme Sunstone, on semble se diriger vers une romance lesbienne,  écrite donc par un mec cis, mais si c'est du niveau de cette dernière, je signe !!

Pour le moment, ça n'existe qu'en anglais, sur Webtoon, mais ça vaut déjà le coup d’œil (ce dernier chapitre 😍)

~~[Vous pouvez le lire sur Webtoon ici.](https://www.webtoons.com/en/challenge/the-queen-and-the-woodborn/list?title_no=502306)~~ 

> [!Warning] Information  
> Il a été retiré de Webtoon pour des raisons inconnues, donc pour le moment c’est indisponible à ce que je sache.