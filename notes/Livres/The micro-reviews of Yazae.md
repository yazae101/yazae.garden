---
title: The micro-reviews of Yazae
aliases: 
date: 2023-08-18
edit: 2024-06-22
notetype: feed
lang: en
tags:
  - Livres
  - Queer
description: You’ll find here all my reviews written in english on the fediverse who are about books 📚
---

You’ll find here all my reviews written on the fediverse who are about books and written works of art in general 📚  

There is a french version of this with other book review written in french, [[Les micro-critiques de Yazae|you can find it here]].

## Index

[TOC]

### Wildflower (20/08/23)

![[Wildflower]]

### Malice T01 (08/02/23)

![[Malice T01]]

### Blood Witch - Blood Canticles T01 (03/02/23)

![[Blood Witch - Blood Canticles T01]]

### The Big Book of Post-Collapse Fun - The Planetary Tarantella T01 (28/11/22)

![[The Big Book of Post-Collapse Fun - The Planetary Tarantella T01]]

### Level 99 - Re-reading notes (29/12/21)

![[Level 99 - Re-reading notes]]