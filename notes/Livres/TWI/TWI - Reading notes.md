---
title: TWI - Reading notes
aliases: 
notetype: unfeed
lang: en
date: 2022-07-30
edit: 2023-09-11
tags:
  - Livres
  - TWI
  - LitRPG
  - Fantasy
description: Reading notes of TWI
"":
---

Hi !  
I will try to keep reading notes of my readings of The Wandering Inn, some times just for a joke or to express why it's so good, other times to ramble about thoeries, it's a looong webnovel who update very often, so it will makes a lot of posts.

It *may* have spoilers, but, honestly, if it's the first time you read about this series, you are so far from me that, it's safe, in a sense, you won't remember/understand before you caught up. (anyways, will specify the spoilers).

## Index

[TOC]

## Volume 1

### 1.25 - Rewrite (25/05/23)

Soo, I'm slowly reading the rewrite of volume 1, when I feel like it. It's a bit hard because I must have re-read this volume at least 5 times in 2 years, so it's a bit too much :')

Anyways, the new elements in the story, new perspectives and all makes it all worth it, even if yeah, I take my time :)

One of the main change are the Antinium and Goblin's perspective. There is a lot more context, akin to the later volumes, where the first one was almost exclusively centered on Erin. I was following the discussions of the readers, and some think that having Rags's perspective isn't that useful / can lessen the deed of Erin wanting to make peace with goblins.

Like, before that, for the reader *and* the people of the world, goblins are monsters and Erin's actions are seen as foolish / dangerous.  
On the rewrite, we have a bit of Rags' s perspective, so we are already empathetic toward her, and Erin's actions makes more sense, and are less original /special /whatever.

I honestly don't know what is better for new readers, it's obviously savoury to old readers like me to get bits and glimpses of one of my favourite character of the series in the first volume, and the world feels more coherent and lively than before to me.  
It's the same with the Antinium, a lot of non-canon things had been stripped in this first volume (Antinium weren't as well defined as today), and we get a bit of Pawn & Bird's perspective I think? before they had a name, and yeah it lessen maybe the revelations of the next volumes, but it open a lot of new mysteries and questions too, so it makes the book feels more coherent and in place than before I think.

I do hope that the end of vol1 will be a bit less depressing than before, but I don't think it will change much, vol1 is so dark when you compare it to the whole series, almost as dark as vol.8! :')

In my first read of TWI, I almost read the 2 first volumes in a weekend, so I didn't really felt the weight of this volume, but when I tried the audiobook, it was definitely somber and almost hard to listen sometimes. When I think of TWI, I think mostly of the joyful pace and silly sheningans, and it's easy to forget that before Erin was well in place in her Inn, times were harsher ^^  
(well, not really, there is still dark themes in later volumes, a lot actually, but there is way more perspectively and parallel stories, and Erin's storyline is often a bol of fresh air, the dark themes are more diluted than in vol.1 where Erin is still trying to survive.)

Anyways, yeah, it's a good rewrite I feel, vol1-2 were really different than the rest, now vol1 feels more whole, it's good o/

## Volume 5

### Chapter 5.59 (15/08/23)

(audiobook \#9)  
There was a looot of fighting and grim tones in this book, almost all of it, but before the end we’ve got a whole chapter of peace and laughter, and god it helps a lot !

It really are these chapters that save this part of the story, otherwise I would be too depressed to read through and would skip it (I almost did !).

I feel that the audiobook is worse than the text version because you’v got all the grim tone out loud and played, and you can’t skip through easily. And you *feels* for your characters and it’s amazing and terrible in the same time.

But to be honest, when I was reading this volume for the first time, I had my first "burn out" of reading of TWI. Way too much to gobble in one reading or even in a week, and I did try that…

So yeah, lot’s of goblins tales in this book/volume, and goblins are usually depressing, it’s PirateAba who tells it, not me !

### Interlude - Garry & Pebblesnatch

It's one of the best & funnier chapter of this book :33  
A cooking competition 2 days before a war - only PA could have wrote that :3

## Volume 7

### Chapter 7.45 (20/02/21)

Vampires, Fierre sickness - Situational spoilers, some theories

I'm at 7.45 now o/

I have rushed the 2-3 last chapters till dawn to get the conclusion, I really really love Ryoka and Fierre and couldn't stop without it ! <img src="../assets/img/emojis/Mrsha_Dead.png" alt="emoji Mrsha dead" width="5%"/>

I will have to re-read this chapter because I skipped the Horns parts, but at least I can stop my reading for a week and go back to work :')

I'm really in hate with Yawles right now, so infuriating, let the sick vampires live you jerk.

And really wondering why Ryoka isn't' top quality blood to Fierre, because the fae magic, the absence of levels, the lack of sentiments? T_T  
———  
If I re-read TWI someday, I will pick some storyline and compile them without the rest, unless it already exist somewhere🤔

### Chapters 7.58-59-60 (06/03/2021)

Assassins - situational spoilers, theories

This whole arc was really cool, there is no way Ryoka should have survived to all of that but it's Pirate's way of storytelling, and I don't mind at all ^^

Fierre hunger theory confirmed : must eat people with level to become cheated, wonder if she is still under the level cap ot can get all the classes she want with a lot of time.  
I'm sad that Ryoka can't be first choice meal for Fierre though i_i  

Can't wait she eat a bit of royalty to see what's doing, Lyon regal blood donator, you could have a business here o/  

Hum so there is 2/3 five family involved in the party now, I guess Tyrion and Maviola can call favors to another one, we will see!  

### Chapter 7.61 (08/03/21)

Erin big event - big spoilers  
**⚠️ Big spoiler starting now ⚠️**

Wow, I didn't know Erin will die so soon (I had gathered that on some message that I read here on the serv), I was expecting it around the summer solstice. I have NO IDEA what could be written next with this event, everything else will become almost irrelevant compared to that.  
Also, worst death ever :')

Starting an alternate Universe were Fierre transform Erin into a vampire to save her, and she become the first vampire innkeeper in centuries. She doesn't spit blood, she actually drink it, beware if you don't pay your tab, you could end on the menu!

**⚠️ Big spoiler ending ⚠️**

### Solstice part 1-9 (10/03/21)

Faeries - spoilers & theories

Okay, amazing ending, even if it's still extremely bitter, I really love all the immortal's arcs and the faery world is really well described. And Ivolethe 💜

**⚠️ Spoilers starting now ⚠️**  
So the faeries and the old folks can't help to beat the gods, and it will be Ryoka and some Goblins who will have to try, if it's even possible… With a lot of earthers to mess up even more with Innworld. Good luck to Pirate with that :')

I still wonder about the 'do not interfere' rule of the fae, it doesn't appear to be an oath made with the gods, the two seems to be sworn enemies.

Yet if the gods aren't all-powerful, it seems difficult to think that they are responsible for the level system🤔

Okay, maybe with full power they were able, even a sleeping god can produce Crelers in his sleep, but it's still strange. And where do you place the God's Era chronologically? In the time of Wyrms and gnomes? Of Elvens? Does the gods have made them fall or hide? Are they responsible for the fading of true magic or just are the by-product of civilisation?

So much questions, so few answers, even after 7+ million words, this books never cease to amaze me ^^  
**⚠️ Spoilers ending ⚠️**

## Volume 8

### Chapter 8.00 (20/03/21)

The saddest chapter in history, it's hard u_u  
First time I don't want to binge-read TWI  
———  
Okay, the end was a lot less depressing than the start :3

Love Sylveran and Ulvama, the Erin Effect™️ still change the world even now !  
Thanks Pirate for giving us hope and purpose like your characters, you are kind like that <img src="../assets/img/emojis/Mrsha_Heart.png" alt="emoji Mrsha heart" width="5%"/>

### Chapter 8.03 (21/03/21)  

Ryoka & the Inn - no spoilers, lack of context
> “Okay. Automatic safety mechanism. Good to know.”
>
> Good thing stupidity transcended realities or the makers wouldn’t have created this function.

I'm dying :')  
———   
> “Silly Goblin. When the fire goes away, you light it again.”
>
> The flame grew.

Literally crying right now <img src="../assets/img/emojis/Mrsha_Cry.png" alt="emoji Mrsha crying" width="5%"/>

### Chapter 8.06 TR  (22/03/21)

Knights - no spoilers
> “Spring is youth! Energy! We can train or fight or run all day—at least, if you embody our order!”
>
> He barked across the groaning [Knights], who’d heard the speech before.
>
> Rabbiteater nodded.
>
> “Must be great for sex.”

Good one, Rabbiteater 👌

### Chapter 8.09  (23/03/21)

Fetopeh - no spoilers
> After repeat viewings of the highlight reel—and nearly 17 hours of watching Fetohep of Khelt being amazing, Toren decided he wanted to become Fetohep when he grew up.

I'm dead <img src="../assets/img/emojis/Mrsha_Dead.png" alt="emoji Mrsha dead" width="5%"/> xD

### Chapter 8.13F (25/03/21)

AAAAAH FIERRE IS SOOOO COOOL !!!!!!  
The Vampire [Blood Bank Manager], best job ever

New adventuring team : Marsha the White And Mighty, Ulvama the Temptress Shaman and Fierre the Vampire Ninja ! I want to read this fanfic ! (or non-fan fic even :3)

### Interlude : Perspective and Past (09/10/21)

Ghosts of the past - after 8.46G

**Background**  
(background spoilers)

So, this chapter is an Interlude. In TWI, this means a break in the main timelines, with focus on a side characters/or, often too, glimpses of stories from differents characters we know, around a theme.

The chapter start in the lands of the dead, and focus on ghosts of the past. It's very interesting, because we know very little of the metaphisic of this world, of it's huuuuge history, because we have the knowledge of mortals who live right now, on this era, where there is very few immortals left, and this civilisation have a memory of a few thousand of years, at most, which is not enough.

We know very little about the era of Giant and Dragons, even less about Elves and Gnomes (who appears to have vanished), but we know that, at some time, they were Gods in this world, and now they are dead (and one probably 'sleeping').

But, how? When? Do they have build this world or the world's system (levels & and classes)? If not, who? We are at Volume 8, and there is still so much questions!

So, to help little with these mysteries, we get some historical facts in this chapter :

> What made even Merindue hesitate was…it sounded like Nerrhavia was serious. And yet—yes, remember the tyrant. Remember her acts and evil that led them to denounce her, and celebrate her demise.
>
> Also remember she was devious enough to rule an empire for nearly a thousand years, and that she did not want to die twice.

Nerrhavia is a ghost, former queen of a country of Stitch Folks. In the world today, we know of a country of Stitch Folks : Nerrhavia's Fallen. So, she ruled it for about 1000 years, and her death, caused by Merindue (?), even changed the name of the country to celebrate her fall.

We learn later the secret of her longevity:

> Draining ten thousand men of life the hard way?

It's very possible that she was a vampire, but maybe not ? 🤔

We know in chap 6.14K that her reign ended 600 years ago, so she reigned between -600 and -1600~.

- Another discovery of this chapter :

A ghost general , Ignoyeithe, talking about the dead gods :

> Now consider their _nature_. They are all anathema. I have felt such revulsion only a few times. I do not know the ‘Crelers’ which came after me; but the Soulless of Rhir? The things that come from beyond, that Drath hunts? And…A’ctelios Salash itself.  
> These all provoke the same universal hatred in my being. Do you understand?

So, it's very interesting because a link is made between all these beings : the fact that everyone perceives them as 'anathema', something to destroy at all cost, because they _aren't of this world_. We also learn several things :

- The general don't know the Crelers, some monstruous foe who appeared in Rhir around -6000, and caused a global war of 800 years. So, he lived before - 6000.

- Before the Crelers were the Soulless of Rhir, another big enemy who come from Rhir's depht. We know that there is a dead god in Rhir who made the Crelers and the Soulless and many other horrors, while sleeping.

- Drath, a mysterious island we don't really know yet, fight 'things' from 'beyond'. From memory, Drath is close to the 'end of the world', a fall without end who circle the world of TWI, so I guess that 'beyound' means 'from the end of the world'.

- A'ctelios is a giant dead being who have fallen eons ago. It has apparently come from 'beyond the end of the world' too. Today, it's head is conolized by humans, who carve through it's immortal flesh to build their city. The problem : it begin to regain some kind of life through the millennia, and regularly must be 'stopped' from awakening (even with a carved head).

So, we can guess that all these beings are linked together, being considered as 'gods' or creation of 'gods', and they are all perceived as 'anathema'. They also are outsiders of this world, maybe invaders, they seems to want to prey on the living, or to take revenge? Mysteries.

### Chapter 8.50 (03/11/21)

After 8.49M (Mrsha and the Meeting of tribes) - Background spoilers

So, this chapter is starting with Pelt, a (very) grumpy and (relatively) old dwarf. He manage to get the Demas metal from the Meeting of tribes, and : amazing, he didn't know this metal at all!!  
He is a master of his craft, one of the best smith of the whole world, he have to be at least 300 years old and comes from the best forges of his kind, so, if he didn't knew this metal, it is the same as saying that the metal was created, invented by a master.

He recon that it's a feat of a lifetime for any [Smith] or [Melter], but say an interesting thing :

> So it’s been made before. If it was a metal like Naq-Alrama steel. If it was a metal made out of meteorites and Adamantium. If an [Archmage] helped discover it, and it came shooting out of legends, rare ores never seen under the light of the sun—whenever a [Smith] makes it, they’re geniuses and damned lucky and it’ll be a grand thing. But they always, always _rediscovered_ it. Understand?

> “No. The greatest smiths to ever walk this world made every metal conceivable. And you can laugh. You can think I’m talking on forge-fumes. But I am not wrong. I could take this dagger, and if I went to…I would be able to come back and tell you exactly what it does, how it’s made, and why it’s a piece of worthless metal because it was forged so poorly, and smelted with no understanding of it.”
>
> Pelt lifted the dagger. He looked at Emessa and the apprentice saw it in his gaze. The Dwarf turned away.
>
> “Don’t ask me. Just know, apprentice. You’re young.”

Sooooo, a mysterious being, a people (?), very probably immortal, but _not_ the Dragons : it left the Elves and the Gnomes, or another species I don't know. The Elves have vanished, and it's the gnomes who were known to be the crafter among the other immortals.

But…. Pelt knew them ??? Did he met them in his lifetime?!  
Even Teriarch, our favorite dragon, talk about them as a people of the past, from several millenia ago, at the very least.

Maybe a gnome is hiding in the mountains of the dwarfs?? Does the dwarfs are coming from the gnomes like Pelt could be hinting (when saying that dwarves were not that short, at least _today_)? Like the Drakes and the Dragons, the Lizarfolk and the Wyrms?

### Chapter 8.60 (26/12/21)

The Chatroom - Big spoilers when said.

Utterly hilarious, it takes place mainly in a magical chat, where all the people interested in Erin's cure are being gathered.

We have monarchs of several kingdoms, a Great Sage, archmages, famous alchemists, lyonette, Mrsha, Ryoka, a Necromancer, several members of the Horns, a grand revenant Vizir, all of them talking together, picking fight, exchanging news, and at first it's totally unreadable because there is too much people in the chat, with a lot of random pseudonyms ^^

It's basically all the main storylines of Volume 8 who comes together in one chat, and it's very rewarding in this matter.

It takes some time for them to divide in sub chats by subject, and actually begin to pool knowledge, not without some battle of ego between mortals and immortals, all of them great in their fields and yet unable to find a solution by themselves. Ceria (IceSquirrel ) introduce some knowledge about cooling flesh from her Cryo-master, the Goblin Witch and alchemist I talk about ghost antidote.

The necromancer and Khelt talk about the damage made to the body while de-freezing someone (Erin discover then that actual people were used for experiment by Khelt)

**⚠️ Big spoiler starting now ⚠️**

All in all, everyone make progress, and some characters enter the field unprompted, Belavierr make a scene when discovering Mrsha, calling her her nemesis in the chat, Geneva (the doctor) appear to give a solution to a missing piece of the problem, and, last but not least, Nier enter the field with his ghost mushroom from recently befriended goblins, and resolve one of the main issue with his habitual class.

The pace of the story and of this quest is quickening, in such ways that we actually could see Erin's resurrection at the end of Vol 8, in… a few months? :O

After that, I believe that we will begin the last arc of the story, the battle against the gods… we shall see!

**⚠️ Big spoiler ending ⚠️**

What a trip <3

### Interlude - Satar (20/02/22)

The Writer - After 8.65 and the Hectval war interlude - Background spoilers

This chapter is really funny, like every chapter with self-insert in it : it’s about a [Writer], and you KNOW that the skills Satar is getting (let’s say, like, [Anti-Cramping Muscles]) would _really_ be useful for the writer of this very story :’’)

But it’s not only funny, it’s getting more and more intense, Satar is reaching some BIG secret with her skill [Narrative: Spot Inconsistencies], and it’s such a good use of a skill, it’s awesome, everything PA touch turn to **magic** and it’s yet another example of that :3

I will continue to read but, just, AAAAAAHHH it’s so cool !!!!!!!!!!!

———  

>“What is it? A new Skill about the past?”  
> […]
>
> “It’s…[Seal of Veracity].”  
> […]  
> _Seal? So cool! Can you show me?_
>
> Mrsha danced about excitedly, looking away from her [Message] scroll and penpal. Obligingly, Satar reached out and drew…a magical stamp out of the air. Mrsha went cross eyed, and Satar pressed it into Mrsha’s forehead.
>
> The magical stamper glowed and vanished, leaving a little, glowing stamp on Mrsha’s forehead.  
> […]
>
> “It lasts as long as it’s true, Mri. I think it changes color if it’s less true. Go on! Try it!”
>
> Mrsha frowned. Then she wrote, carefully, and held up a notecard for all to see.
>
> _I am always a good girl._
>
> **Instantly, the seal glowing a gentle green-blue, and even slightly changing to hues of yellow or purple—turned red, burst into flames, and fell off Mrsha’s forehead.**  
> The girl stared down at it.

I’m _dying_

———  
few, what a ride… again!! I love this series so much💜💜💜

## Volume 9

### Chapter 9.00 (23/07/22)

Quests

> "Erin is back. Yay, yay. I wonder what will explode first?”

> "Did you see that smile? Your Ancestors, it’s happening"
>
> "Alright. I’m confirming with Zevara, and I’ll send a copy of the [Message] to the Council. The betting pool wins—ten days! Did she say where she was going?”  
> “Um…Adventurer’s Guild. A ‘little errand’.”  
> “Dead gods have mercy on them.”

It would be funny if nothing were happening, with everyone overreacting :')  
But the truth is that they are all underreacting, you can't control the Solstice Effect TM :3


> “Patrol 1 is in place, Watch Captain. Solstice Contingency active.”

One of the funniest line so far :''''')

———  
It's a very good introducing chapter for the volume 9, with crazy Erin sheningans, a lot of fun, and some plot too, TWI is back and it will be good and magic as promised :3

### Chapter 9.03 (31/07/22)

Politics, After Interlude - Singing ships

So Erin get back to work, and, even if she is afraid, she is starting to get seriously involved with powerful people. You can't change the world without a bit of politics when you'r an innkeeper, and she start at least to understand that.

It's good to see that she can be really thoughtful and beat everyone expectations in the same time (Nerrhavia, in your face!) ^^

She managed to beat Niers and the lord of the rain in romance, in politic **and** in chess, such a power move :')

### Interlude - Mundanity and Memorials (31/07/22)

Statues, after 9.08, situational spoilers

> “Look at the door. She’s opening it! Look at the snow! It’s at that inn. The one that kills people.”

:')

———  
The Antinium are cool, but what Garry did to his shop is supercool, feeding poor customers high quality meals for coppers - even without bugs in it!

It makes him level, so the Hive finance it, such a win-win situation :3

———  
**⚠️ Spoiler starting now ⚠️**  

> The Goblin was clawing at his throat and chest. Actually clawing—Erin saw him tearing at his shirt. He was trying not to see, but his eyes were locked on the tallest figure. She looked back in alarm.
>
> “Numbtongue? What’s wrong?”
>
> “Who is—who is that?”
>
> His voice cracked. The mists were still obscuring her—that was the only thing keeping him sane. He didn’t want to see. Something was screaming at him—a roar of so many voices, so deep down he feared it. Like the background noise of his soul. Erin turned. Then she saw what he was staring at.
>
> “Sprigaena. The last Elf—”

OMG  
So, Goblins really are linked to Elfs, another part of the mystery !👀

**⚠️ Spoiler ending ⚠️**  

### Chapter 9.19 E (02/10/22)

Witches (3)  
Feeew, again, what a ride!  
Starting the reading at 4AM didn't help, but it was such a good chapter!

It concludes the 3-parts interlude of witches started at 9.10 W, a really great mini-arc with one of the most cool and magical subject of Innverse :3

I must admit, I have mixed feelings with the 6th volume, it was so _long_ and I burnt myself a bit reading it, but it was also the volume of witches, witches as memorable and wonderful as Pratchet's, no less.

We have the occasion to see them again here, after so long, interacting with Erin (!), and it's such a good match !😍  
TWI reaches one of it's peak when it comes to legends of old, magic and Erin, so it totally makes sense to bring them together x)

And the very idea of a law elemental was 👌👌👌

Anyways, huh, I will get some sleep now x_x

### 9.32 (18/01/23)

OMG some huuuuuge secret this chapter, getting a real glimpse of the system, so interesting :3

---

IT WAS SUCH A HUGE CHAPTER !!!!!!!!!!!!!!!!

Pirate’s rest was worth it, we got a chapter of 38 000 words and it advance the plot in a huge manner, reveal big secrets, and unlock possibilities for the future who weren’t there a chapter ago, all of that with the usual bit of magic and wonder, really, well played.

AND THAT’S IT, LGBT INN ACTIVATED, I KNEW IT ! It took 3 more volumes than I had guessed, but we got there, with a really good trans character who got their due development at last :3

### 9.41 (pt.2) (13/04/23)

The Order of Solstice - Part 2. contextual spoilers.

Okay, it’s been a while since I talked about TWI, because, you know, not everything have to be about this wonderful book, and I have the feeling I parrot myself a little, with every chapter being amazing and grand and *magic*.

I mean, it’s true, every chapters are at least very entertaining, a lot are amazing, but it gets a bit old after the 100th chapter described that way, I figure.

Anyways… t’was an amazing chapter for sure T_T

It’s an arc about knighthood, the good heroic kind, and mostly about the Order of Solstice. Normen finding a way to do things right, the embodiment of a new kind of knights : the Order of Solstice, who gather low-life people with honor, everyone with enough heart and who wants to believe in a cause.

And when this arc collide with christmas, little girl who ask Santa to get her village food, Mrsha the [Emberholder], Cat skeleton with a chariot of potatoes at his back as a Rodolf substitute, and an army of Snow Golem to fight with magic fire, you know it’s been a good chapter :’)

Of course, a bit obvious, a bit of a *déjà-vu* (we already had an arc like that the last winter, 4 volumes back I think ?), but with new evil to fight and new heroes rising to the challenge!
