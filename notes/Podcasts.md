---
title: Podcasts
aliases:
  - podcasts
notetype: feed
date: 2023-07-13
edit: 2024-01-14
tags:
  - Militantisme
  - Podcasts
  - Liste
  - Queer
description: Une liste de podcasts que j’écoute régulièrement, par sujet.
---

Une liste de podcasts que j’écoute régulièrement, par sujet.

Retrouvez aussi mes [[Podcasts - Notes d’écoute|Notes d’écoute]] si ça vous intéresse.

## Index

[TOC]

## Queer

### Interlope

<img src="/assets/img/notes/podcasts/interlope_logo.jpg" alt="Cover du podcast Interlope" width="40%"/>

> Interlope – Une émission transpédégouine féministe, par et pour les intru-es 

**Période de diffusion :** 2022-2024 (en cours)  
**Fréquence :** Mensuelle (24 épisodes)  
**Format :** Long (1h+)  
[Site](https://blogs.radiocanut.org/interlope) • [Flux RSS](https://blogs.radiocanut.org/interlope/author/interlope/feed/)

### Le Feu, la Rage, l’Orage

<img src="/assets/img/notes/podcasts/leFeuLaRageLOrage_logo.jpg" alt="Cover du podcast Le feu, la rage, l’orage" width="40%"/>

> Une émission queer et féministe en mixité choisie MeufsTransPédésGouines, un mardi par mois à partir de 20h sur Radio Pikez!

**Période de diffusion :** 2018-2021 (arrêté)  
**Fréquence :** Mensuelle (20 épisodes)  
**Format :** Long (1h+)  
[Site](http://www.pikez.space/le-feu-la-rage-lorage/) • [Flux RSS](https://hearthis.at/set/104510-9006691/podcast/)

### Chroniques Mutantes

<img src="/assets/img/notes/podcasts/chroniquesMutantes_logo.jpg" alt="Cover du podcast Chroniques Mutantes" width="40%"/>

> Emission queer, riot grrrlz, transpédégouine, anarchiste, punk et féministe

**Période de diffusion :** 2015 - 2024 (en cours)  
**Fréquence :** Hebdomadaire (plus 380 épisodes)  
**Format :** Moyen (1h)  
[Site](https://www.radiopanik.org/emissions/chroniques-mutantes-/) • [Flux RSS](https://www.radiopanik.org/emissions/chroniques-mutantes-/podcasts.rss)

### Ingenrables

<img src="/assets/img/notes/podcasts/ingenrables_logo.jpg" alt="Cover du podcast Ingenrables" width="40%"/>

Un podcast sur la non-binarité par des personnes concernées, l’épisode sur la parentalité non-binaire (\#2) est très intéressant.

> Le podcast de la non-binarité de genre par deux personnes non-binaires, Sacha (ou J4K) et Ezra Tellington. Nous allons parler autant de sexualité que de parentalité en passant par les liens entre non-binarité et neurodiversité, et tous les sujets qui peuvent concerner le genre ! Masquer
>
> Retrouvez-nous sur Twitter (@Ingenrables) pour suivre nos créations et les actualités de ce podcast.

**Période de diffusion :** 2022 (en pause ?)  
**Fréquence :** Mensuel (3 épisodes)  
**Format :** Moyen (45min - 1h)  
[Site](https://spectremedia.org/ingenrables/) • [Flux RSS](https://spectremedia.org/feed/podcast//ingenrables)

<!-- |   |   |
|---|---|
|**Diffusion**|2022 (en pause ?)|
|**Fréquence**|Mensuel (3 épisodes)|
|**Format**|Moyen (45min - 1h)|| Période de diffusion  | 2022 (en pause ?)|
|[Site](https://spectremedia.org/ingenrables/)|[Flux RSS](https://spectremedia.org/feed/podcast//ingenrables)| -->

### Meufs à Teubs

<img src="/assets/img/notes/podcasts/meufsATeubs_logo.jpg" alt="Cover du podcast Meufs à Teubs" width="40%"/>

Une émission par et pour des meufs trans, qui mêle intime et politique. Très cool.

> Le podcast qui se réapproprie les sexualités, corps et vécus transféminins.
>
> C'est compliqué d'avoir un rapport sain à nous-même dans un monde dans lequel un corps transfem c'est soit un objet de fantasme, soit une horreur. Dans lequel nos vécus sont des vécus minoritaires, dont on ne peut pas parler ouvertement. Nous, on veut en parler. Nos vécus n'auront aucune prétention ou volonté à représenter la totalité des personnes transfem, mais on espère que nos fiertés, nos histories et nos doutes parlent à d'autres personnes.


**Période de diffusion :** 2022 - 2023 (en cours)  
**Fréquence :** Irrégulier (6 épisodes)  
**Format :** Moyen - long (1h - 1h30)  
[Site](https://anchor.fm/meufs-teubs) • [Flux RSS](https://anchor.fm/s/a2e14018/podcast/rss)

### Nos Voix Trans

<img src="/assets/img/notes/podcasts/nosVoixTrans_logo.jpg" alt="Cover du podcast Nos Voix Trans" width="40%"/>

Militantisme et transidentités, par Jena. <3

> Chaque épisode, je demande à une personne trans, qui milite ou qui a milité, comment elle fait pour vivre, survivre, et, si possible, être heureuse face au public, à nos opposants politiques et transphobes, ou au contact de nos “alliés”, dans les associations généralistes ou même dans nos familles. L’objectif est de nous motiver à agir et militer, et à trouver les outils et techniques afin de rester actives et militantes, tout en prenant soin de nous.

**Période de diffusion :** 2021 - 2023 (en cours)  
**Fréquence :** Irrégulier (15 épisodes)  
**Format :** Moyen - long (30min - 1h30)  
[Site](https://nosvoixtrans.fr) • [Flux RSS](https://anchor.fm/s/4614c9cc/podcast/rss)

### Un Podcast Trans

<img src="/assets/img/notes/podcasts/1PCT_logo.jpg" alt="Cover du podcast Un Podcast Trans" width="40%"/>

Discussions entre potes trans, très cool 💜

> On bavarde, on se redécouvre, et puis on est trans ✨
>
> Un Podcast Trans est une conversation mensuelle entre ami·es trans. Souvent invisibles, nos échanges du quotidien ont une grande valeur militante, qu’ils soient des plus engagés ou des plus innocents.  
> Alors nous souhaitons rendre visible nos rires et nos pleurs, pour nos adelphes et pour celles et ceux qui nous découvrent. 💜🏳️‍⚧️

**Période de diffusion :** 2021 - 2023 (en pause)  
**Fréquence :** Mensuel (20 épisodes)  
**Format :** Long (1h - 2h)  
[Site](https://1pct.fr) • [Flux RSS](https://anchor.fm/s/dd0734/podcast/rss)

### Gouinement Lundi

<img src="/assets/img/notes/podcasts/gouinementLundi_logo.jpg" alt="Cover du podcast Gouinement Lundi" width="40%"/>

Une émission lesbienne assez cool, de qualité et sujets variables mais à la fréquence fixe.

> Gouinement Lundi est une émission de radio qui prend le haut des ondes féministes, portée par les paroles des concerné·es en non-mixité choisie chaque 4ème du lundi du mois sur Fréquence Paris Plurielle (106.3 FM).

**Période de diffusion :** 2015 - 2023 (en pause)  
**Fréquence :** Mensuel (90 épisodes)  
**Format :** Moyen (1h)  
[Site](https://gouinementlundi.fr/) • [Flux RSS](https://gouinementlundi.fr/feed/podcast/)

### Voyage au Gouinistan

<img src="/assets/img/notes/podcasts/voyageAuGouinistan.jpg" alt="Cover du podcast Voyage au Gouinistan" width="40%"/>

> Elles sont lesbiennes, elles sont en couple et elles sont journalistes.
>
> Christine Gonzalez et Aurélie Cuttat vous emmènent au Gouinistan ! Embarquez pour ce voyage inédit où nos narratrices racontent les hauts et les bas de leur parcours lesbien. Itinéraire prévu : des escales dans leurs familles, chez leurs ami·es, et des rencontres avec des personnalités fortes qui tracent les contours de la culture lesbienne en Suisse et alentour.

**Période de diffusion :** 2023 (terminé)  
**Fréquence :** Publié en reportage (20 épisodes)  
**Format :** Moyen (30 min)  
[Site](https://details.rts.ch/podcasts-originaux/programmes/voyage-au-gouinistan/) • [Flux RSS](https://www.rts.ch/podcasts-originaux/programmes/voyage-au-gouinistan/podcast/?flux=rss)

### Totally Trans Podcast Network

<img src="/assets/img/notes/podcasts/totallyTrans_logo.jpg" alt="Cover du podcast Totally Trans" width="40%"/>

> In Searching for the Trans Canon, Katie Coleman, Ada-Rhodes Short and Henry Giardina discuss finding trans representation in film, tv, and literature.

**Période de diffusion :** 2020-2024 (en cours)  
**Fréquence :** bimensuelle ou plus (40+ épisodes)  
**Format :** Moyen-long (30 min-1h+)  
[Site](https://totallytrans.buzzsprout.com) • [Flux RSS](https://feeds.buzzsprout.com/1534219.rss)

## Féminisme

### DégenréE

<img src="/assets/img/notes/podcasts/degenree_logo.jpg" alt="Cover du podcast DégenréE" width="40%"/>

> DégenréE - l’émission féministe pour déranger !
> 
> Une émission féministe : actualité, analyses, témoignages, infos, débats, points de vue, musiques etc. de femmes, de lesbiennes, de trans et autres monstres !

**Période de diffusion :** 2002 - 2024 (en cours)  
**Fréquence :** Bimensuelle (plus 260 épisodes)  
**Format :** Moyen - long (1h+)  
[Site](https://campusgrenoble.org/) • [Flux RSS](https://campusgrenoble.org/?feed=podcast&podcast_series=degenree)

### Sexualité

#### Entre nos lèvres

<img src="/assets/img/notes/podcasts/entreNosLevres_logo.jpg" alt="Cover du podcast Entre nos lèvres" width="40%"/>

Des entretiens autour d’une nouvelle personne à chaque fois, qui parle de sa sexualité et de son intimité. Souvent des meufs au micro, host par des meufs, assez cis et blanc mais ça se diversifie quand même pas mal. CW annoncés en début d’épisode.

> Des portraits intimes qui racontent les vraies histoires autour de la sexualité (mais pas que).

**Période de diffusion :** 2018 - 2023  
**Fréquence :** Mensuel (plus de 60 épisodes)  
**Format :** Moyen (45min - 1h)  
[Site](https://www.entrenoslevres.fr/) • [Flux RSS](https://feed.ausha.co/b235QuRk2vxB)

#### Polycule·s

<img src="/assets/img/notes/podcasts/polycules_logo.jpg" alt="Cover du podcast Polycule·s" width="40%"/>

> Le podcast francophone qui s'intéresse à toutes les pratiques polyamoureuses, sous un prisme transpédégouine et militant.
**Période de diffusion :** 2023-2024 (en cours)  
**Fréquence :** Mensuelle (3 épisodes)  
**Format :** Moyen-long (45 min-1h)  
[Site](https://polycules.fr) • [Flux RSS](https://anchor.fm/s/df01aa4c/podcast/rss)

### Travail du sexe

#### Catégorie : Trans

<img src="/assets/img/notes/podcasts/categorieTrans_logo.jpg" alt="Cover du podcast Catégorie : Trans" width="40%"/>

Un podcast par et sur des personnes trans et TDS, un·e invité·e par épisode.

> Conversations entre personnes transgenres travailleuses du sexe, que ça soit en ligne ou en réel.

**Période de diffusion :** 2022 - 2023 (en cours)  
**Fréquence :** Mensuel (4 épisodes)  
**Format :** Moyen (30 - 45 min)  
[Site](https://anchor.fm/categorie-trans) • [Flux RSS](https://anchor.fm/s/d2a22204/podcast/rss)

#### Le Putain de podcast

<img src="/assets/img/notes/podcasts/putainDePodcast_logo.jpg" alt="Cover du podcast Le Putain de Podcast" width="40%"/>

Un podcast très intéressant sur le travail du sexe, sorti en 2019.

> Le Putain de Podcast est une émission bimensuelle par et pour les Travailleurs du sexe. Pour les novices, le travail du sexe est le terme employé pour tout les métiers qui gravitent autour de la sexualité, prostitution, camgirl, actrices X, etc…  
> 
> Comme son nom l'indique ce podcast se focalise tout particulièrement sur les putains. 

**Période de diffusion :** 2019 (arrêté)  
**Fréquence :** Bimensuelle (6 épisodes)  
**Format :** Moyen (1h)  
[Site (1er épisode)](https://castbox.fm/ch/2089922) • [Flux RSS (1er épisode)](https://rss.castbox.fm/everest/acdf630a223c4763b93260571957c7e7.xml)  
[Site (épisodes 3-6)](https://soundcloud.com/user-496417702) • [Flux RSS (épisodes 3-6)](https://feeds.soundcloud.com/users/soundcloud:users:620211801/sounds.rss)

#### Hétaïre

<img src="/assets/img/notes/podcasts/hetaire_logo.png" alt="Cover du podcast Hétaïre" width="40%"/>

Un podcast québécois sur l'escorting, principalement. 2 parcours de vie de travailleureuses du sexe.

> Cette série immersive est un témoignage de travailleuses et travailleurs du sexe, une tribune pour un métier trop souvent stéréotypé. Ces histoires sont les leurs, rien n'a été romancé.

**Période de diffusion :** 2018-2019 (arrêté)  
**Fréquence :** Bimensuelle (11 épisodes)  
**Format :** Moyen (1h)  
[Site](https://www.choq.ca/balados/hetaire) • [Flux RSS](https://www.choq.ca/emissions/hetaire/rss)

## Handicap

### Autos Podcast

<img src="/assets/img/notes/podcasts/autosPodcast_logo.jpg" alt="Cover du podcast Autos" width="40%"/>  

Podcast-témoignage très cool sur l'autisme.

> Un podcast sur la découverte tardive d'une condition autistique, par Law Esculape.

**Période de diffusion :** 2019-2021 (terminé)  
**Fréquence :** Irrégulier (6 épisodes)  
**Format :** Court-Moyen (20-40min)  
[Site](https://soundcloud.com/laura-esculape)• [Flux RSS](https://feeds.soundcloud.com/users/soundcloud:users:634697391/sounds.rss)

### En tant que telle - Pair-aidance en santé mentale

<img src="/assets/img/notes/podcasts/enTantQueTelle_logo.png" alt="Cover du podcast En tant que telle" width="40%"/>

[description à venir]

[Site](https://entantquetelle.com/) • [Flux (soundcloud)](https://soundcloud.com/en-tant-que-telle/tracks)

## Écologie

### Avis de Tempête

<img src="/assets/img/notes/podcasts/avisDeTempete_logo.jpg" alt="Cover du podcast Avis de Tempête" width="40%"/>  

Podcast intéressant sur les différentes luttes écologistes, toujours en intersection avec d’autres luttes.

> Avis de Tempête est un podcast axé sur les sujets d’écologie politique et de luttes sociales.
>  
> Le podcast aimerait apporter des clés de compréhension de ce monde, un monde qui ne cesse de détruire nos milieux de vie, nos liens et nos existences. On aimerait raconter nos expériences politiques et nos combats, nos joies et nos espoirs.


**Période de diffusion :** 2022-2024 (en cours)  
**Fréquence :** Régulier (30+ épisodes)  
**Format :** Court-Moyen (30min-1h)  
[Site](https://audioblog.arteradio.com) • [Flux RSS](https://audioblog.arteradio.com/blog/177155/rss)

## Militantisme

### Penser les luttes

<img src="/assets/img/notes/podcasts/penserLesLuttes_logo.jpg" alt="Cover du podcast Penser les luttes" width="40%"/>

> Lutter, c’est savoir articuler la théorie et la pratique. Nous interrogeons des intellectuel·les, des militant·es chevronné·es, des artistes ou des chercheur·ses. Logiques, stratégies, histoire et sociologie des mouvements sociaux, une pluralité des approches pour analyser les luttes en profondeur. Et pour apprendre à lutter, ensemble.

**Période de diffusion :** 2020-2024 (arrêté ?)  
**Fréquence :** bimensuelle ou plus (54+ épisodes)  
**Format :** Moyen-long (45 min-1h)  
[Site](https://radioparleur.net/) • [Flux RSS](https://feed.ausha.co/BqmVUKpdXNDB)

## Culture pop, Geek

### Internet Exploreuses

<img src="/assets/img/notes/podcasts/internetExploreuses.jpg" alt="Cover du podcast Internet Exploreuses" width="40%"/>
    
> Internet Exploreuses, c'est l'émission des cultures numériques, un programme très sérieux à propos d'un truc qui à la base ne devait pas l'être : Internet. Lucie Ronfaut et Héloise Linossier co-présentent Internet Exploreuses pour ORIGAMI. 

**Période de diffusion :** 2024 (en cours)  
**Fréquence :** Mensuelle (1 épisode)  
**Format :** Long (2h)  
[Site](https://linktr.ee/origamilive_) • [Flux RSS](https://cloud.origami.ng/ie/podcast_origami-internet-exploreuses.xml)

### ABCD - Pop culture et progéniture

<img src="/assets/img/notes/podcasts/ABCD_logo.jpg" alt="Cover du podcast ABCD - Pop culture et progéniture" width="40%"/>

Un podcast sur la parentalité geek, deux meufs cool en host, des invité·es assez variés (mais pas très queer).

> Le podcast pour parents irresponsables.  
> Pop Culture, progéniture (et aussi du JDR).
>
> Présenté par Diraen et Force Rose.

**Période de diffusion :** 2016 - 2020 (terminé)  
**Fréquence :** Mensuel (plus de 30 épisodes)  
**Format :** Long (2h+)  
[Site](https://zqsd.fr/abcd-podcast/) • [Flux RSS](http://zqsd.fr/abcd.xml)

### ATFQ - Alors, t'as fait quoi ce week-end ?

<img src="/assets/img/notes/podcasts/ATFQ_logo.jpg" alt="Cover du podcast ATFQ - Alors, t’as fait quoi ce week-end ?" width="40%"/>

Un podcast de discussion sur des produits culturels, des même meufs que ABCD.

> Le podcast des recommandations culturelles de Diraen et Force Rose.

**Période de diffusion :** 2018 - 2022 (terminé)  
**Fréquence :** Bimensuel (60 épisodes)  
**Format :** Court (20 - 30min)  
[Site](https://zqsd.fr/atfq-podcast/) • [Flux RSS](http://zqsd.fr/atfq.xml)

### Le Cosy Corner

<img src="/assets/img/notes/podcasts/cosyCorner_logo.jpg" alt="Cover du podcast Le Cosy Corner" width="40%"/>

Conversation détente entre deux potes & anciens journalistes jeux-vidéo, plutôt cool et relaxant. Un des seuls podcast tenu par des mecs cis qui me fait pas souffler toute les 5 minutes (même si ça arrive quand même).

> Le podcast de tous les p'tits cosys, par Medoc et Moguri.  
> On y cause de pop-culture de manière fortement aléatoire, mais toujours détendue.

**Période de diffusion :** 2016 - 2022 (en cours)  
**Fréquence :** Bimensuel (plus de 120 épisodes)  
**Format :** Long (2h+)  
[Site](https://soundcloud.com/lecosycorner) • [Flux RSS](https://feeds.soundcloud.com/users/soundcloud:users:274829367/sounds.rss)

### Les Amazones

<img src="/assets/img/notes/podcasts/amazones_logo.png" alt="Cover du podcast Les Amazones" width="40%"/>

Une émission de radio sur la culture pop, par des meufs et des personnes queer, sujets et cast différent à chaque fois, qualité et visions politiques variables mais toujours cool à écouter.

> La culture geek au féminin.

**Période de diffusion :** 2018 - 2023 (en cours)  
**Fréquence :** Mensuel (plus de 220 épisodes)  
**Format :** Moyen (1h)  
[Site](https://www.choq.ca/balados/les-amazones) • [Flux RSS](https://www.choq.ca/emissions/les-amazones/rss)

### Wait For It…

<img src="/assets/img/notes/podcasts/WFI_logo.jpg" alt="Cover du podcast Wait For It" width="40%"/>

Un podcast sur les séries TV, assez intello et host par deux mecs cis, mais intéressant et des genres de séries assez variés.

> Le podcast qui aime regarder les séries TV, et penser avec elles.
>
> Présenté par Christophe Butelet et Yann François.

**Période de diffusion :** 2019 - 2023 (en cours)  
**Fréquence :** Irrégulier (20 épisodes)  
**Format :** Long (1h30 - 2h30+)  
[Site](https://zqsd.fr/wait-for-it/) • [Flux RSS](http://zqsd.fr/wfi.xml)

### ZQSD

<img src="/assets/img/notes/podcasts/ZQSD_logo.jpg" alt="Cover du podcast ZQSD" width="40%"/>

Podcast jeux vidéo par des journalistes jv, un format convivial. Plus de 10 ans de jeux-vidéos, assez cool. Une seule meuf régulière, Force Rose, qui cohostait ABCD et ATFQ.

> Le podcast mensuel sur les jeux vidéo PC réalisé par l’équipe qui a coulé le magazine Joystick.
>
> Présenté par Jika, Deez, Force Rose, Gruth, Hoopy, Ianno, Savonfou et Walou.

**Période de diffusion :** 2013 - 2023 (en cours)  
**Fréquence :** Irrégulier (Environ 100 épisodes)  
**Format :** Long (2h+)  
[Site](http://www.zqsd.fr) • [Flux RSS](http://zqsd.fr/zqsd.xml)

## Tech

### Libre à vous

<img src="/assets/img/notes/podcasts/libreAVous_logo.jpg" alt="Cover du podcast Libre à vous" width="40%"/>

Émission de radio sur le logiciel libre et les libertés informatiques. Plusieurs sujets, toutes les semaines.

> Libre à vous ! est l’émission de radio proposée par l’April sur la radio Cause Commune, « la voix des possibles ».
>
> Prenez le contrôle de vos libertés informatiques et suivez l’actualité du Libre. Retrouvez les dossiers politiques et juridiques traités par l’April, les échanges avec des personnes invitées, et bien entendu de la musique sous licence libre.

**Période de diffusion :** 2018 - 2023 (en cours)  
**Fréquence :** Hebdomadaire (plus 160 épisodes)  
**Format :** Moyen - long (1h30)  
[Site](https://www.libreavous.org/) • [Flux RSS](https://www.libreavous.org/rss)
