---
layout: default
title: Home
---
# Yazae<span class="emphase-alt">.</span>
*Un blog perso où je code, j’écris et assemble des p’tits bouts d’trucs.*

Bonjour ! Vous voici sur mon blog / Digital Garden ! Il est encore en travaux (et le sera sans doute toujours), mais y’a déjà quelques pages fonctionnelles ! Allez voir mes [notes](/notes/), mon [répertoire de liens](/tisser-des-liens-sur-la-toile/) ou mon [À propos](/a-propos/) pour commencer à visiter ce jardin pas si secret <span aria-hidden="true">;)</span><span class="v-hidden">Émoji clin d’œil</span>