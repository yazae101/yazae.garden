---
layout: default
---
# {{ title }}
<article>
  <header>
    {%- if lang == "en" -%}
      Note published the {{ date | date: "%d-%m-%Y" }}.
      Edited the {{ edit | date: "%d-%m-%Y" }}.
    {%- else -%}
      Note publiée le {{ date | date: "%d-%m-%Y" }}.
      Éditée le {{ edit | date: "%d-%m-%Y" }}.
    {%- endif -%}
    {%- if tags != null -%}
    <ul class="note-tags">
      <p>
        <b>Tags :</b>
      </p>
      {%- for tag in tags -%}
        <li><a href="/tags/{{ tag | slugify }}" class="note-tag">{{ tag }}</a></li>
      {%- endfor -%}
    </ul>
    {%- else -%}
      
    {%- endif -%}
  </header>
  <hr>
  {{ content }}
</article>
{%- include "backlinks.html" -%}