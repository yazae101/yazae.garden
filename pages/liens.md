---
layout: default
title: Tisser des liens sur la toile 🧶
aliases:
  - Friends & cool places
date: 2023-04-17
edit: 2023-08-20
description: Une liste de liens cool sur les interwebs.
---
# {{ title }}

Une liste de liens cool sur les interwebs.

## Index  
[TOC]

## Chez les camarades

- [azaliz.me](https://azaliz.me/)  
  🇫🇷 · 🇬🇧  
  Le digital garden d’Azaliz, une personne cool qui parle de bouquins, de queerness et de podcasts. Mention spéciale à ses [réécritures de chansons](https://azaliz.me/tags/musique.html) !
- [oniricorpe.eu](https://oniricorpe.eu/) [(gemini)](gemini://oniricorpe.eu "lien gemini du site, à accéder avec un navigateur spécialisé")  
  🇫🇷 · 🇬🇧  
  Le site d’Émy, artiste/saboteuse, on peut y lire des textes cool, des blogs sur le handicap et la queerness, et [un article très utile sur le bidouillage des liseuses kobo](https://oniricorpe.eu/log/tech/2022-10-08_kobo-hacks-mods).
- <a class="img-link" href="https://www.aphrodite.dev/"><img src="/assets/img/pages/aphrodite.gif" alt="A badge showing Aphrodite’s symbol on the left, and the progressive pride flag on the right." width="88" height="31"></a> [(aphrodite.dev)](https://www.aphrodite.dev/)  
  🇬🇧  
  Le site perso en anglais d’Aphrodite/Artemis, sur le handicap, la queerness, le développement/la tech.  
  Mention spéciales à ses [histoires érotiques (mais pas que)](https://www.aphrodite.dev/~writings/) !

- [Merry Portenseigne](https://merry.eu.org/)  
  🇫🇷  
  Un blog d’une personne trans & catholique, très sympas.

- [vegaelle.fr](https://vegaelle.fr/)  
  🇫🇷  
  Un blog de recette de cuisine vegan fait par une chouette personne.

- [Simply Denise](https://blog.simplydenise.eu){lang=en}  
  🇬🇧 · 🇫🇷   
  Le blog de [Denise](https://simplydenise.eu/), une dev/adminsys trop forte qui parle principalement de code et de tech, en anglais et en français, mais aussi du [cauchemard de l’administration française quand on est pas dans la norme](https://blog.simplydenise.eu/posts/joindre_l_administration__c_est_une_deviation__partie_1_.html).

- [Le blog de Shad](https://shad.pipou.academy/bludit)  
  🇫🇷  
  Le blog de [Shad](https://pipou.academy/@shad), admin de la très cool instance mastodon [pipou.academy](https://pipou.academy).

- [blog.canevas.eu](https://blog.canevas.eu/)  
  🇫🇷  
  Le blog de [rhapsodos](https://toot.aquilenet.fr/@rhapsodos), une très chouette personne qui parle de tech, de bouffe et de politique.

- [Blog.shift.gay](https://blog.shift.gay/)  
  🇬🇧  
  Le récent blog de [shebang](https://catcatnya.com/@shebang), une "cutie qui parle de tech" (c’est factuel). Elle a écris [un article sur l’éthique dans la tech, en anglais](https://blog.shift.gay/posts/left_1_free_2/).

## D’autres endroits sympas

- <a class="img-link" href="https://cadence.moe"><img src="/assets/img/pages/cadence_now.png" alt="The text &quot;cadence now!&quot; on a purple background. There is a moon-shaped logo on the left side and a tiny star in the bottom right." width="88" height="31"></a>  [(cadence.moe)](https://cadence.moe/)  
  🇬🇧

- [Cypherweavers Cafe Webring](https://arteneko.github.io/cypherweavers.cafe/)

- [DNS Witch Collective](https://dns-witch.eu.org/)  
  🇫🇷 · 🇬🇧  
  Un collectif qui aide à obtenir un nom de domaine en `.eu.org` gratuitement. Héberge aussi [Grimoire](https://grimoire.eu.org/about), une istance WriteFreely, qui intègre ActivityPub. [June](https://june.lacoloc.cafe/), une des cofondatrice du collectif, est aussi la personne à l'origine de l'[EnergyDrink Licence](https://github.com/june-coloc/EnergyDrink-license) !

- [blog.potate.space](https://blog.potate.space/)  
  🇫🇷  
  Un site-ressource sur le transféminisme, très intéressant.

- [Sylpheline toujours dans la lune ou dans les nuages](https://sylphelinetoujoursdanslaluneoudanslesnuageswordpre.wordpress.com/)  
  🇫🇷  
  Un blog sur l'antivalidisme et la lutte des classes, assez vénère, intéressant.

- [La lutine du web](https://www.lalutineduweb.fr/)  
  🇫🇷  
  Un blog d’une intégratrice web et spécialiste en accessiblité, très sympas et intéressant. Un article [à lire sur l’accessibilité de l’écriture inclusive](https://www.lalutineduweb.fr/ecriture-inclusive-accessibilite-solutions/#point-median-accessibilite), parmis tant d’autres.

### Jeux vidéos

#### Clickers cools

- [Démonology](https://demonology.agate.blue/)  
  🇬🇧  
  Récoltez toutes les âmes de votre monde pour la cause du mal, mais faites gaffe à pas anéantir la vie dans le process.
 
- [Universal Paperclips](https://www.decisionproblem.com/paperclips/index2.html)  
  🇬🇧  
  La production de trombones, vers l'infini et l'au-delà !   

### Pub gratuite

Des personnes très cool qui vendent des trucs très cool.

#### Artisanat

- [Loki Gwynbleidd Vintage Propaganda](https://lokivintagepropaganda.com/)  
  🇫🇷 · 🇬🇧  
  Le site d’un artiste anar 🏴 très cool qui réemploi des techiques traditionnelles pour donner un effet "d’époque" / vintage à ses créations militantes.  
  Il y a une boutique sur le site, et sinon tous les visuels sont disponibles sous licence CC BY-NC-SA sur son [pixelfed](https://pixelfed.fr/Lokigwyn).

- [From otter space](https://fromotterspace.fr/)  
  🇫🇷  
  La boutique d'une personne très sympas qui est sur le fédiverse, et qui fait des bijoux en maille.
  Réseaux : [mastodon](https://piaille.fr/@from_Otterspace) et [ailleurs](https://linktr.ee/FromOtterspace).

#### Bouquins

- L. Rowyn [ladyrowyn.com](https://ladyrowyn.com/)  
  🇬🇧  
  Un.e auteurice très sympas qui écris des histoires de SSF avec beaucoup de polyamour dedans.
  Mention spéciale à [Level 99](/notes/the-micro-reviews-of-yazae/#level-99---re-reading-notes-(29%2F12%2F21)){lang=en} que j'ai beaucoup aimé.

- Veo corva [veocorva.xyz](https://veocorva.xyz/)  
  🇬🇧  
  Un.e auteurice qui écris des histoires de fantasy très chill pour la plupart, avec des personnes queer, neuroa, handis, racisés super cool.
  J'ai vraiment adoré son [Non-Player Character](/notes/les-micro-critiques-de-yazae/#non-player-character){lang=en}.

- Lizzy Crowdagger [crowdagger.fr](https://crowdagger.fr/)  
  🇫🇷  
  Ecrivaine hollistique, Lizzy écris des histoires de gouines punk sorcières et de vampires trans, entre autres <3

### Trans stuff

- [administrans.fr](https://administrans.fr "https://administrans.fr/")  
  🇫🇷  
  Un site pour faciliter les démarches administratives du changement de prénom.

- [wikitrans.co](https://wikitrans.co/)  
  🇫🇷  
  Un site de ressources pour les transitions, très complet.

---  

Dernière édition le {{ edit | date: "%d-%m-%Y" }}.
