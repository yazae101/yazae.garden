---
title: Blood Witch - Blood Canticles T01
aliases: 
date: 2023-02-03
edit: 2024-06-22
notetype: feed
tags:
  - Livres
  - Critique
  - Queer
  - Fantasy
description: Review of Blood Witch - Blood Canticles T01.
lang: en
---

**Blood Canticles T01 - Blood Witch**, by Naomi Clark.  
**CW / themes :** Urban Fantasy, Lesbian romance, Witchcraft, Demons

![Cover of Blood Witch, written by Naomi Clarke.Two white women sensually close to each other, in a graveyard background](/assets/attachments/reviews/bloodCanticlesT1_cover.jpg =350x)

OK I know the cover is kinda clickbait and not really interesting (a typical lesbian surnatural romance really) (others books of the author are worse :p), BUT it was a good read?

I was ranting about not finding enough queer main characters in urban fantasy, while all the discourse about monsters and outcasts (not the Wednesday kind ty) were pretty queer in itself, well, here it is! 

Some tension and murders, a bit of lesbian romance, blood magic, sultry demoness - please take my money!  
Tbh, not a huge twist of the genre, cops aren’t bastards here and it lack of other representations, but it was a fun read, and that's just what I was asking for :)