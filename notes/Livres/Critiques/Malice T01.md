---
title: Malice T01
aliases: 
date: 2023-02-08
edit: 2024-06-22
notetype: feed
tags:
  - Livres
  - Critique
  - Fantasy
  - Queer
lang: en
description: Review of Malice T01.
---

**Malice**, vol 1 of the Malice duology, by Heather Walter.  
**CW / themes :** Fantasy, Yong Adult, LGBT, Lesbian romance, Fairy Tales

![Cover of Malice. There is two feminine hands bound by a rose with lots of needle next to the title.](/assets/attachments/reviews/malice_cover.jpg =350x)  

So, it’s a complete rewrite of Sleeping Beauty, we aren’t really in a fairy tales setting but in a brand new fantasy world, with Humans, a Fae species and "dark" fae one called Vila, who have been wiped out in the last war.

Our main character, Alyce, is a half-vila, and is despised by all in the city. Contrary to the other "half-breeds" called the Graces, her powers are nefarious and wicked, she can’t heal or make someone beautiful, but she can kill, poison or grow warts on your face.

She lives in the human society, where fairy magic is hugely controlled by the power, and where the magic users are kept in golden cages, in service to the nobility & rich people, and without the right to leave the country or refuse any service, at the cost of their health.

There is also a curse in this story, of course : centuries ago, one of the last Vila cursed the crown by vengeance, such as all the descendants of the Queen are dying at their 21th birthday if they didn’t find TrueLove!™️ yet.

Alyce will realize slowly that her heritage is deeper than she thought, she will find a friend in the cursed princess, or perhaps even more, and they could have found a happily ever after… unless the world around them is structured to take advantage of their powers and strip them of their agency, unless politics and bigotry can get in the way of love, unless Alyce finally becomes the villain everyone expects her to be. Who knows ?

~

It was a really exciting read, and a heck of a ride, I’m not fully recovered yet ! The finale end in a hell of a cliffhanger, and I can’t wait to read the 2nd volume to know how all of this will be concluded !

I really hope that the characters will find a kind of happy ending, but it will be really hard to get after the ending of this volume, we shall see 🙂