# Installation d’un serveur web - Nginx

Nginx est un programme puissant et versatile qui permet de gérer les services web hébergés sur votre serveur. Il succède à Apache, serveur web historique. Caddy est aujourd’hui un succèsseur de Nginx plus simple à paramètrer et plus moderne (il gère les certificats https directement, peut servir du jekyll ou autre de manière automatisée, etc.), mais j’ai d’abord essayé Nginx pour pouvoir faire des tests de performance entre Nginx et Caddy, et choisir celui qui me conviens le mieux.

## Index  

{Insérer TOC}

## Installer Nginx

Traduction de ce tutoriel (+ notes perso).  
https://www.linuxcapable.com/how-to-install-nginx-on-fedora-linux/

Update les paquets fedora :
```bash
sudo dnf upgrade --refresh
```

Installation :
```bash
sudo dnf install nginx
```

Check si c’est bien installé :
```bash
nginx -v
```

## Ouvrir les ports http(s) (80 et 443)

http utilise le port 80, tandis que https utilise le port 443. On utilise les preset de `firewall-cmd` pour plus de "simplicité".  
```bash
sudo firewall-cmd --permanent --zone=public --add-service=http

sudo firewall-cmd --permanent --zone=public --add-service=https
```
Le paramètre `--permanent` permet de conserver les règles édictées au redémarrage de firewall-cmd. On peut ne pas ajouter ce paramètre pour tester les options, et potentiellement ne pas conserver les modifications.

On redémarre `firewall-cmd` pour être sûr que c’est bien pris en compte :
```bash
sudo firewall-cmd --reload
```

## Vérifier l’installation de Nginx

Check si nginx est lancé :
```bash
systemctl status nginx
```

Dans le cas où le nginx n’est pas lancé, le lancer :
```bash
sudo systemctl enable nginx --now
```
Le paramètre `enable` de `systemctl` permet de lancer le programme au démarrage, tandis que `--now` sers à [vérifier].  
Si on préfère lancer nginx une seule fois (sans toucher aux fichiers de démarrage), on peut faire `systemctl start nginx`.

Si `systemctl status nginx` donne autre chose que `enabled` et `active (running)`, et montre des messages d’erreur, c’est qu’il y a un problème dans l’installation. Cela ne devrait pas se produire avec un serveur et Nginx fraichement installé, mais en cas de problème, voir [ici].

Voici le résultat que j’obtiens de mon côté :
```bash
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; preset: disabled)
     Active: active (running) since Mon 2023-04-03 17:52:35 EDT; 2h 54min ago
    Process: 44702 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 44703 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 44704 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 44705 (nginx)
      Tasks: 5 (limit: 2085)
     Memory: 4.7M
        CPU: 300ms
     CGroup: /system.slice/nginx.service
             ├─44705 "nginx: master process /usr/sbin/nginx"
             ├─44706 "nginx: worker process"
             ├─44707 "nginx: worker process"
             ├─44708 "nginx: worker process"
             └─44709 "nginx: worker process"

avril 03 17:52:35 pc-127.home systemd[1]: Starting nginx.service - The nginx HTTP and reverse proxy server...
avril 03 17:52:35 pc-127.home nginx[44703]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
avril 03 17:52:35 pc-127.home nginx[44703]: nginx: configuration file /etc/nginx/nginx.conf test is successful
avril 03 17:52:35 pc-127.home systemd[1]: Started nginx.service - The nginx HTTP and reverse proxy server.
```

- En cas d’erreur affichée, il faut parcourir le fichier de log situé dans `/var/log/nginx/error.log`, aller à la fin du fichier (erreur la plus récente), et essayer de comprendre le problème. Une recherche internet permet en général de trouver la solution.

- On peut aussi utiliser la commande `journalctl -xeu nginx.service` pour avoir une précision sur les messages d’erreur dédiés au service nginx (quand le service ne démarre pas).

- Si aucun autre service web n’est lancé en même temps que nginx, on peut tester d’accèder à MonIpLocale et MonIpExterne depuis un navigateur.

	Il est très possible que l’IP externe ne soit pas accessible. Si votre serveur accède à internet via une box, il est possible que le firewall interne à la box bloque vos ports. Voir [le chapitre dédié](Fedora%20server.md#Paramètrer%20une%20IP%20fixe%20pour%20notre%20serveur%20sur%20la%20box%20internet).

	Si tout fonctionne, on devrait avoir accès à une page type : [refaire le screen soit-même]  
	![](nginx-test-page-working-on-fedora-linux.png)

## Paramètrer Nginx en CLI

Sur fedora, le fichier de config de Nginx est situé dans `/etc/nginx/nginx.conf`.  
On l’ouvre avec nano, en `sudo` pour pouvoir le modifier :
```bash
sudo nano /etc/nginx/nginx.conf
```

```nginx
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log notice;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    # Ajout de charset utf-8 pour forcer l’utf-8 partout
    charset utf-8;
    # augmentation du hash pour une raison obscure
    server_names_hash_bucket_size 64;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    
    ### EDIT HERE ###    
    
    # include /etc/nginx/conf.d/*.conf;
    # On ajoute sites-enabled/*.conf pour pouvoir gérer les config de chaque serveur séparément et les activer / désactiver à la volée
    include /etc/nginx/sites-enabled/*.conf;
}
```

On a supprimé tout ce qui concernait les serveurs et ajouté la dernière ligne `include /etc/nginx/sites-enabled/*.conf;`, on doit maintenant créer les répertoires et le fichier de config pour notre serveur :

`sudo mkdir /etc/nginx/sites-available/ ; sudo mkdir /etc/nginx/sites-enabled/`

`sudo nano /etc/nginx/sites-available/vadia.eu.org.conf`

Dans le fichier `vadia.eu.org.conf`, on va pouvoir mettre le paramètrage du serveur :

```nginx
    server {
        listen       80;
        listen       [::]:80;
        server_name  vadia.eu.org www.vadia.eu.org;
        root         /var/www/vadia.garden;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

On a changé la ligne root [répertoire par défaut], pour mettre le répertoire qui contiendra nos sites et services web. Apache avait `/var/www/html` par défaut, et donc l’usage de mettre les sites web dans `/var/www` est resté, mais il n’y a pas de problèmes particulier à choisir un autre emplacement.

Pour vadia.eu.org, on va choisir `/var/www/vadia.garden`, car c’est mon blog / digital garden.

On enregistre, et on ferme nano.

Il faut ensuite créer un lien symbolique de `/etc/nginx/sites-avalable/vadia.garden.conf` vers `/etc/nginx/sites-enabled/` :

`ln -s /etc/nginx/sites-avalable/vadia.garden.conf /etc/nginx/sites-enabled/`

J’explique les liens symboliques [juste après](#lien-symbolique-vers-www).

Puis, si ce n’est pas déjà fait, on va créer ou transférer le dossier `vadia.garden` dans `/var/www`.

```bash
sudo cp -r /home/[user]/vadia.garden /var/www/vadia.garden
```
(j’ai copié le dossier vadia.garden qui était dans `/home/[user]/vadia.garden` vers `/var/www`)

On relance nginx pour qu’il prenne en compte les modifications
```bash
sudo systemctl restart nginx
```

### Lien symbolique vers `~/www/`

Pour libérer de la place sur `/` et pouvoir accéder au répertoire de mon site en FTP sans créer un compte root, j’ai finalement créé un lien symbolique de `~/www/vadia.garden` vers `/var/www/`.

C’est avec la commande `ln -s` :

`ln -s ~/www/vadia.garden /var/www/`

Ça donne ça dans `/var/www` :
```bash
total 4
drwxr-xr-x.  2 root root   26  5 mai   11:29 .
drwxr-xr-x. 21 root root 4096 21 mars  14:40 ..
lrwxrwxrwx.  1 root root   27  5 mai   11:29 vadia.garden -> /home/[user]/www/vadia.garden
```
Bon à savoir : j’avais fait une typo dans mon premier lien symbolique (wwww au lieu de www), et ça apparaissait en rouge dans la console, signe que le lien est cassé, mais je n’y avais pas fait attention et ça m’a grillé au moins 2h de débug.

Il faut s’assurer ensuite que le dossier `~/www/vadia.garden` ait bien les permissions de $USER :

`sudo chown -R $USER:$USER ~/www/vadia.garden`  
et  
`sudo chmod -R 755 ~/www/vadia.garden`

## Autoriser Nginx à utiliser le répertoir avec SELinux
{À rédiger}

## Debug UTF-8
{À rédiger}

## Debug try_files

- Pour se passer de try_files (un peu avancé)  
	https://www.getpagespeed.com/server-setup/nginx-try_files-is-evil-too


`sudo nano /etc/nginx/sites-available/vadia.eu.org.conf`

https://stackoverflow.com/a/60156592 :
```nginx
location ~ ^(?<path>/.*/)(?<file>[^/]+)/$ {
    try_files $uri $path$file.html $uri/ =404;
}
```

```bash
sudo systemctl restart nginx
```

Mais ça ne résoud pas l’affichage de `/notes.html` quand on saisi l’adresse `/notes/` ou `/notes`.

Une manière de résoudre le problème a été trouvé par @rhapsodos@toot.aquilenet.fr :

```nginx
	location / {
		index /index.html;
		try_files $uri $uri.html $uri/ =404;
	}
	
	location /notes/ {
		index /notes.html;
		try_files $uri $uri.html $uri/ =404;
	}
	
	error_page 404 /404.html;
	location = /404.html {
	}
	
	error_page 500 502 503 504 /50x.html;
	location = /50x.html {
	}
```

## Activer le HTTPS avec Let’s Encrypt SSL et certbot

https://www.linuxcapable.com/how-to-install-nginx-on-fedora-linux/#Additional-Tips

> To enhance the security of your Nginx server, it is recommended to run it on HTTPS using an SSL certificate.
 
On installe certbot :

```
sudo dnf install python3-certbot-nginx
```

Après, on crée le certif SSL :

```bash
sudo certbot --nginx --agree-tos --redirect --hsts --staple-ocsp --email vadia@murena.io -d vadia.eu.org -d www.vadia.eu.org
```

> To fully secure your Nginx server, it is recommended to implement force HTTPS 301 redirects, a Strict-Transport-Security header, and OCSP Stapling. Before executing the commands, adjust the email address and domain name to match your specific needs.

On a donc une redirection automatique de http vers https.

On peut voir le résultat dans `vadia.eu.org.conf`, un serveur https a été ajouté :

```nginx
    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/vadia.eu.org/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/vadia.eu.org/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    add_header Strict-Transport-Security "max-age=31536000" always; # managed by Certbot

    ssl_trusted_certificate /etc/letsencrypt/live/vadia.eu.org/chain.pem; # managed by Certbot
    ssl_stapling on; # managed by Certbot
    ssl_stapling_verify on; # managed by Certbot
```

Maintenant, on va automatiser le renouvellement du certificat. On regarde d’abord si le renouvellement fonctionne bien :

```bash
sudo certbot renew --dry-run
```

Puis on ouvre crontab pour y inscrire un cronjob :

```bash
sudo crontab -e
```

> After accessing your crontab, you must specify the frequency of automatic certificate renewals. It is recommended to check for renewals daily, and the renewal script will only update the certificate if necessary.

```
00 00 */1 * * /usr/sbin/certbot-auto renew
```

C’est ouvert par vim donc c’est la merde pour édit. Je sais plus comment j’ai fait.
> To save the changes made to your crontab, press (SHIFT) and (:) followed by typing (wa) and then exit the editor by typing (qa).

Si on a un message qui n’est pas un message d’erreur, c’est bon.

## Redirection `www.vadia` vers `vadia`

L’accès à [www.vadia.eu.org](www.vadia.eu.org) donnait initialement une erreur car le sous-domaine www n’était pas paramétré dans les DNS de eu.org (géré chez moi par [DNS Witch](https://dns-witch.eu.org/)). Après les avoir averti, iels ont activé les sous-domaine et [www.vadia.eu.org](www.vadia.eu.org) fonctionnait.

Le problème étant que je ne voulais pas que les deux navigations `www.vadia` et `vadia` soient actives en parrallèle, je voulais que `www.vadia` redirige vers `vadia`.

Après pas mal de galère, j’ai compris comment faire : dupliquer le serveur https :

- Un serveur www.vadia.eu.org qui ne fait que rediriger vers vadia.eu.org

- Un serveur principal vadia.eu.org qui gère le reste.

Ça donne ça (serveur https de redirection) :

```nginx
## REDIRECTION WWW -> NDD
server{
	server_name	www.vadia.eu.org;
	return		301 https://vadia.eu.org$request_uri;

#   IDK if all these options are necessary for a https redirection. It works :shrug:.
    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/vadia.eu.org/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/vadia.eu.org/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    add_header Strict-Transport-Security "max-age=31536000" always; # managed by Certbot

    ssl_trusted_certificate /etc/letsencrypt/live/vadia.eu.org/chain.pem; # managed by Certbot
    ssl_stapling on; # managed by Certbot
    ssl_stapling_verify on; # managed by Certbot
}
```

Et le serveur principal :

```nginx
## MAIN SERVER
server {
	server_name	vadia.eu.org;
	root		/var/www/vadia.garden;

	location / {
		index /index.html;
		try_files $uri $uri.html $uri/ =404;
	}

	location /notes/ {
		index /notes.html;
		try_files $uri $uri.html $uri/ =404;
	}

	error_page 404 /404.html;
	location = /404.html {
	}

	error_page 500 502 503 504 /50x.html;
	location = /50x.html {
	}

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/vadia.eu.org/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/vadia.eu.org/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    add_header Strict-Transport-Security "max-age=31536000" always; # managed by Certbot

    ssl_trusted_certificate /etc/letsencrypt/live/vadia.eu.org/chain.pem; # managed by Certbot
    ssl_stapling on; # managed by Certbot
    ssl_stapling_verify on; # managed by Certbot
}
```

Il y a aussi le serveur HTTP basique qui ne fait que rediriger vers https :

```nginx
## REDIRECTIONS HTTP-> HTTPS
server{
	server_name	vadia.eu.org www.vadia.eu.org;
	listen		80;
	listen		[::]:80;
	return		301 https://vadia.eu.org$request_uri;
}
```

On peut vérifier le bon fonctionnement de la redirection via curl :

`curl -IL https://www.vadia.eu.org`

```bash
HTTP/1.1 301 Moved Permanently
Server: nginx/1.24.0
Date: Fri, 05 May 2023 12:22:34 GMT
Content-Type: text/html
Content-Length: 169
Connection: keep-alive
Location: https://vadia.eu.org/
Strict-Transport-Security: max-age=31536000
```

Ou tout simplement en allant sur [www.vadia.eu.org](www.vadia.eu.org) et en voyant la redirection en direct! :3
