# Licenses

The following licences are attached to this repository. Individual licences and credits may be found inside individual files of this project.

This project is based on Binyamin [eleventy-garden 1.20](https://github.com/binyamin/eleventy-garden/releases/tag/v1.2.0), who was under the MIT Licence :
```txt

MIT License

Copyright (c) 2020 Binyamin Aron Green

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

```

Most of the files have changed and are sublicenced under the Hot Chocolate Licence or CC BY-SA for the content.

```txt

 THE "HOT CHOCOLATE LICENSE ☕" (HCL revision 1312.1):

 yazae@murena.io wrote this file. As long as you retain this
 notice and you are an anarchist/communist who supports oppressed and
 marginalized groups (ie and not exclusively queer folks, BIPOC, disabled
 comrades), you can do whatever you want with this stuff. If we meet some
 day, and you think this stuff is worth it, you can buy me a hot chocolate
 in return.
 Yazae 🏴‍☠️

```
