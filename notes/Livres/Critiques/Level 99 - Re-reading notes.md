---
title: Level 99 - Re-reading notes
aliases: 
date: 2021-12-29
edit: 2024-06-22
notetype: feed
tags:
  - Note
  - Livres
  - Critique
  - Queer
  - LitRPG
description: Review of Level 99 - Re-reading notes.
lang: en
---

**You Thought You Wanted to Be Level 99, But Really You Wanted to Be a Better Person**, of [@Rowyn@mastodon.art](https://mastodon.art/@rowyn). Lang : english.  
**CW / themes :** LitRPG, Polyamour (M-F-F), MMO, love triangle.

![Cover of You thougth you wanted to be level 99 but really you wanted to be a better person. We can see 3 faces, one of a black woman, one of a grey man, and one of a blue-skinned woman. The three are very close to each other. The cover have a text : "A polyamourous gamelit romance"](/assets/attachments/reviews/level99_cover.jpg =350x)

(it’s a reaaly good book, I don’t know why I didn’t really talked about it yet. Maybe I did, but didn’t tagged it or anything, so it’s lost in the aether. Please ask me about it!)

A thing I found really interesting in Level 99 is the way gender of the players differs from the gender of their characters, and the usage of both 'genders' both in the narration and between the players, or even in their inner thoughts.

Exemple :

> Grif was logging in to Guardian again. He'd spent most of his free time…

And :

> [Group] Razgathak: “Are you mocking her?”{talking about Jadaera, the female character controlled by Grif in the game}

Even if the players know themselves IRL, they still gender their characters accordingly, it have a roleplay function but I feel that it's more than that, it's also about respecting the identity the player choose to display?

Anyways, I really like the effect <3

For someone who is misgendered most of the time while I play on my old MMO, even when the players, sometime longtime friends, *know* I’m a trans woman…  
It would be so cool if it was the case in my old communities, but I don’t see it happening in my time.