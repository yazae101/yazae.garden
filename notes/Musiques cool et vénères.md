---
title: Musiques cool et vénères
aliases:
  - Musiques cool et vénères
notetype: feed
date: 2023-09-13
edit: 2023-10-02
tags: Musique
description: Une liste de musiques cool et vénères. La plupart queer et/ou avec des artistes meuf, mais pas que.
---
Une liste de musiques cool et vénères. La plupart queer et/ou avec des artistes meuf, mais pas que.

## Index

[TOC]

### Vices et râlements déviants

> Electro hip-hop et humour cinglant, groupe TPG & féministe

2 titres sur bandcamp : 
- **On recrute**
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=659964083/size=small/bgcol=ffffff/linkcol=0687f5/track=1770101315/transparent=true/" seamless><a href="https://gracevoluptevanvan.bandcamp.com/album/gouines-chien">Gouines à chien de Grace et Volupté Van Van</a></iframe>
- **Hétérophobes**
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=2081731299/size=small/bgcol=ffffff/linkcol=0687f5/track=2447385257/transparent=true/" seamless><a href="https://pandemoscompil.bandcamp.com/album/pandemos">Pandemos de Vices et râlements déviants</a></iframe>
[Le premier album peut être trouvé ici](https://tardigrada.noblogs.org/?p=1023).

	
Coups de cœur sur **Je me souviens**, **Butch Fem** et **Nos grand-mères**.

### Onsind

> Onsind est un groupe folk punk queer de Pity Me, Durham (UK).

Plusieurs albums semblent être sur bandcamp  
- **Heterosexuality is a construct**{lang="en"}
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=3847445414/size=small/bgcol=ffffff/linkcol=0687f5/track=3802796151/transparent=true/" seamless><a href="https://onsind.bandcamp.com/album/dissatisfactions">Dissatisfactions de Onsind</a></iframe>
- **Never trust a Tory**{lang="en"} 
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1685390633/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="https://onsind.bandcamp.com/album/never-trust-a-tory">Never Trust A Tory de Onsind</a></iframe>
  

### Ryaam

Du rap stylé.  
2 album sur bandcamp.
- **La Cible**
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1942229460/size=small/bgcol=ffffff/linkcol=0687f5/track=963963020/transparent=true/" seamless><a href="https://ryaam.bandcamp.com/album/one-mic-2-2">One mic # 2 de Ryaam</a></iframe>
- **Ryaam - Assata Shakur (Yaralé #2)**
  <div class="iframe-container"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/AE6NQKtg4zA?si=eDWBMpmRJL9NDsiF" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></div>

### Grâce et Volupté Van Van

Groupe electro / hip-hop TPG très cool.

[Un album sur Free Music Archive](https://freemusicarchive.org/music/Grace_et_Volupt/Van_Van).  
[Deux autres sur bandcamp](https://gracevoluptevanvan.bandcamp.com/) . 

- **Meute de chats**
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=659964083/size=small/bgcol=ffffff/linkcol=0687f5/track=997696769/transparent=true/" seamless><a href="https://gracevoluptevanvan.bandcamp.com/album/gouines-chien">Gouines à chien de Grace et Volupté Van Van</a></iframe>
- **Geule de gouine**
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=659964083/size=small/bgcol=ffffff/linkcol=0687f5/track=12796262/transparent=true/" seamless><a href="https://gracevoluptevanvan.bandcamp.com/album/gouines-chien">Gouines à chien de Grace et Volupté Van Van</a></iframe>

Coup de cœur sur **Collier de couilles** et **On recrute**.

### NRBC

<img src="/assets/img/notes/musics/NRBC_cover.jpg" alt="Cover de NRBC" width="40%"/>

> **N**ucléaire **R**adiologique **B**actériologique **C**himique : ça c’est pour la ligne d’horizon. Ambiance menace au programme.  
> On fait du rap. Mûri dans des squats, sur des parkings, sous un pont, dans des squares.  
> **N**o **R**eturn **B**astard **C**rew, c’est nous.   
> **N.R.B.C.** c’est du rap, emo-core et classique, tendance iconoclaste.  

[Tout est sur bandcamp](https://nrbc.bandcamp.com/music).
	
- **Comme on peut** 
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/track=1883805590/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="https://nrbc.bandcamp.com/track/nrbc-comme-on-peut">NRBC - Comme On Peut de NRBC</a></iframe>

### GLOSS

<img src="/assets/img/notes/musics/GLOSS_cover.jpg" alt="Cover de GLOSS" width="40%"/>


> GIRLS LIVING OUTSIDE SOCIETY'S SHIT

Punk queercore très cool.

- **We Live**
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1947465492/size=small/bgcol=ffffff/linkcol=0687f5/track=2422164321/transparent=true/" seamless><a href="https://girlslivingoutsidesocietysshit.bandcamp.com/album/trans-day-of-revenge">TRANS DAY OF REVENGE de G.L.O.S.S.</a></iframe>

- **Trans Days of Revenge**
  <iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1947465492/size=small/bgcol=ffffff/linkcol=0687f5/track=1693875055/transparent=true/" seamless><a href="https://girlslivingoutsidesocietysshit.bandcamp.com/album/trans-day-of-revenge">TRANS DAY OF REVENGE de G.L.O.S.S.</a></iframe>

### Lord of the Lost

<img src="/assets/img/notes/musics/LordOfTheLost_cover.jpg" alt="Cover de Lord of the Lost" width="40%"/>

> "Standstill is lethal!" This is the credo of LORD OF THE LOST

Groupe de rock gothique et hardcore allemand, par des gens queer très cool

- **Destruction Manual**<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/track=3182094878/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="https://lordofthelost.bandcamp.com/track/destruction-manual-single-edit">Destruction Manual - Single Edit de Lord Of The Lost</a></iframe>

- **Blood & Glitter**<div class="iframe-container"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5I9CYu668jA?si=egUriTCNQtAph72o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></div>
