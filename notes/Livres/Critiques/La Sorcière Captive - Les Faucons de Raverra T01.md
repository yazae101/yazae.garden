---
title: La Sorcière Captive - Les Faucons de Raverra T01
aliases: 
date: 2022-08-19
edit: 2024-06-22
notetype: feed
tags:
  - Note
  - Livres
  - Critique
  - Fantasy
description: Critique de La Sorcière Captive
---

**La Sorcière captive**, de Melissa Caruso.  VO : anglais, trad FR.  
**CW / thèmes :** Fantasy, Politique, Sorcières, Magie, Bisexualité.  

![Couverture de Les Faucons de Raverra tome 1](/assets/attachments/critiques/lesFauconsDeRaverraT1_cover.jpg =350x)

J'ai lu La Sorcière captive de Melissa Caruso, et c'est une bonne découverte :3

On y suis deux meuf badass à leur manière, très différentes l'une de l'autre, une riche héritière passionnée par la magie et contrainte à faire de la politique, et une sorcière extrêmement puissante & voleuse à la tire à ses heures perdues. Elles sont forcées à collaborer et ne s'entendent pas très bien, mais vont apprendre à respecter l'autre, au fur et à mesure des épreuves qu'elles traverses et des intrigues dans lesquelles elles sont plongées.

L'histoire repose bien plus sur des intrigues politiques que des aventures épiques, mais c'est bien raconté et on crois à l'univers dessiné et aux motivations de chacun.e. Côté romance, la principale est hétero, mais notre sorcière est bi et cela ne pose aucun problème, il y a plusieurs personnages LGBT dans l'histoire et ils sont acceptés et intégrés dans la société comme tout le monde, ça change !

Bref, de la représentation, une histoire qui tiens la route et des personnages attachants, j'ai envie de lire la suite 🙂

(à savoir, c'est une trilogie mais on peut tout à fait se contenter du premier tome, on ne finis pas en cliffhanger ni rien)